package com.ureclub.model.news.WpTerm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Links(@SerializedName("wp:post_type")
                 val wpPostType: List<WpPostTypeItem>?,
                 val curies: List<CuriesItem>?,
                 val about: List<AboutItem>?,
                 val self: List<SelfItem>?,
                 val collection: List<CollectionItem>?,
                 val up: List<UpItem>?):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(WpPostTypeItem),
            parcel.createTypedArrayList(CuriesItem),
            parcel.createTypedArrayList(AboutItem),
            parcel.createTypedArrayList(SelfItem),
            parcel.createTypedArrayList(CollectionItem),
            parcel.createTypedArrayList(UpItem)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(wpPostType)
        parcel.writeTypedList(curies)
        parcel.writeTypedList(about)
        parcel.writeTypedList(self)
        parcel.writeTypedList(collection)
        parcel.writeTypedList(up)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Links> {
        override fun createFromParcel(parcel: Parcel): Links {
            return Links(parcel)
        }

        override fun newArray(size: Int): Array<Links?> {
            return arrayOfNulls(size)
        }
    }
}