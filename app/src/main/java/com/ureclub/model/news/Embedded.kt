package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.ureclub.model.news.WpTerm.WpTerms


data class Embedded(val author: List<AuthorItem>?,
                    @SerializedName("wp:term")
                    val wpTerm: List<List<WpTerms>>):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(AuthorItem),
            TODO("wpTerm")) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(author)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Embedded> {
        override fun createFromParcel(parcel: Parcel): Embedded {
            return Embedded(parcel)
        }

        override fun newArray(size: Int): Array<Embedded?> {
            return arrayOfNulls(size)
        }
    }
}