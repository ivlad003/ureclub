package com.ureclub.model

import android.arch.persistence.room.Embedded

data class UserUp(val firstname: String = "",
                val phone: String = "",
                val facebook: String = "",
                val description: String = "",
                val id: Int = 0,
                val position: String = "",
                val hide_phone: Int = 0,
                val linkedin: String = "",
                val email: String = "",
                  var phones: ProfilePhone?,
                val lastname: String = "")


data class UserUpdate(val user: UserUp)


