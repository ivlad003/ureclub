package com.ureclub.model.filter

data class CategoryItem(var name: String = "",
                        val count: Int = 0,
                        var id: Integer = Integer(0),
                        var isSelected: Boolean = false,
                        val slug: String = "")
