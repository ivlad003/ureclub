package com.ureclub.model.contacts

import android.arch.persistence.room.Embedded
import android.os.Parcel
import android.os.Parcelable
import com.ureclub.model.ProfileCompany
import com.ureclub.model.ProfileLinks
import com.ureclub.model.ProfilePhone
import java.io.Serializable
import java.util.ArrayList

data class PeopleItem(val firstName: String = "",
                      val lastName: String = "",
                      val phone: String = "",
                      val description: String = "",
                      val company: Company?,
                      val links: Links?,
                      val id: Int = 0,
                      val priority: Int = 0,
                      val position: String = "",
                      val type: String = "",
                      val dateSince: String = "",
                      val categories: ArrayList<Integer>?,
//                      val phones: ProfilePhone?,
                      val email: String = ""): Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Company::class.java.classLoader),
            parcel.readParcelable(Links::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readArrayList(Integer::class.java.classLoader) as ArrayList<Integer>?,
//            parcel.readParcelable(ProfilePhone::class.java.classLoader),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(phone)
        parcel.writeString(description)
        parcel.writeParcelable(company, flags)
        parcel.writeParcelable(links, flags)
        parcel.writeInt(id)
        parcel.writeInt(priority)
        parcel.writeString(position)
        parcel.writeString(type)
//        parcel.writeParcelable(phones,flags)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "PeopleItem(firstName='$firstName', lastName='$lastName', phone='$phone', description='$description', company=$company, links=$links, id=$id, priority=$priority, position='$position', type='$type', email='$email')"
    }


    companion object CREATOR : Parcelable.Creator<PeopleItem> {
        override fun createFromParcel(parcel: Parcel): PeopleItem {
            return PeopleItem(parcel)
        }

        override fun newArray(size: Int): Array<PeopleItem?> {
            return arrayOfNulls(size)
        }
    }


}