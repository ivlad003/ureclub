package com.ureclub.model.newsNew

import java.util.*

data class NewsItem(val date: String = "",
                    val image: String = "",
                    val post: Int = 0,
                    val ids: List<Integer>?,
                    val id: Int = 0,
                    val categories: List<CategoriesItem>?,
                    val title: String = "",
                    val content: String = "")


data class CategoriesItem(val name: String = "",
                          val id: Int = 0)


data class NewsModel(val code: Object? = null,
                val data: List<NewsItem>?)


