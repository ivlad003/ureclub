package com.ureclub.activitys

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import com.ureclub.App
import com.ureclub.R
import com.ureclub.model.Profile
import kotlinx.android.synthetic.main.splash_screen.*

class Splash : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
        AsyncTask.execute {
            val profile: Profile? = App.Companion.db.profileDao().getProfile()
            runOnUiThread {

                val settings = PreferenceManager.getDefaultSharedPreferences(App.context)
                val editor = settings.edit()
                val lang = settings.getString("language","")
                lang?.let {
                    if (it.isEmpty()) {
                        editor.putString("language", "uk")
                        editor.putInt("languageIndex", 1)
                        editor.commit()
                    }
                }

                if (profile != null) {
                    root.postDelayed({
                        startActivity(Intent(this, Main::class.java))
                        finish()
                    }, 500)
                } else {
                    root.postDelayed({
                        startActivity(Intent(this, Login::class.java))
                        finish()
                    }, 500)

                }
            }
        }
    }
}