package com.ureclub.adapters
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ureclub.R
import com.ureclub.model.news.ModelNews
import com.ureclub.model.newsNew.NewsItem
import com.ureclub.viewHolders.ViewWithOutSectionHolderNews

class NewsWithoutSectionAdapter(private val newsItems: List<NewsItem>, private val listener: (NewsItem) -> Unit):
        RecyclerView.Adapter<ViewWithOutSectionHolderNews>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewWithOutSectionHolderNews(
            LayoutInflater.from(parent.context).inflate(R.layout.news_item,
                    parent, false))

    override fun onBindViewHolder(holder: ViewWithOutSectionHolderNews, position: Int)  =
            holder.bind(newsItems[position],listener)

    override fun getItemCount() = newsItems.size
}