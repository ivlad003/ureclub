package com.ureclub.fragments.members

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.adapters.*
import com.ureclub.fragments.PeopleProfile
import com.ureclub.model.FilterMembers
import com.ureclub.model.MembersCalendarHeader
import com.ureclub.model.contacts.CompaniesModel
import com.ureclub.model.contacts.ContactsModel
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.model.event.EventModel
import com.ureclub.model.filter.CategoryItem
import kotlinx.android.synthetic.main.members_list.*


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response



class Companies : Fragment() {
    var membersCalendarHeaderList: ArrayList<MembersCalendarHeader> = ArrayList<MembersCalendarHeader>()
    var listsFilterEvent= FilterMembers(CategoryItem(), java.util.ArrayList())
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.members_list,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadData(listsFilterEvent)
        list.layoutManager = LinearLayoutManager(context)

        tryAgain.setOnClickListener {
            loadData(listsFilterEvent)
        }

        search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filter(p0.toString())
            }
        })
    }

    private fun loadData(filterMembers: FilterMembers) {
        containerInfo.visibility = View.GONE
        progress.visibility = View.VISIBLE
        val token = "${App.profile.token}"
        App.Companion.api.allContacts(token)
                .enqueue(object : Callback<ContactsModel> {
                    override fun onFailure(call: Call<ContactsModel>?, t: Throwable?) {
                        containerInfo.visibility = View.VISIBLE
                        progress.visibility = View.GONE
                    }

                    override fun onResponse(call: Call<ContactsModel>?, response: Response<ContactsModel>?) {
                        containerInfo.visibility = View.GONE
                        progress.visibility = View.GONE

                        val model = response?.body()
                        model?.data?.let { data ->


                            val listAdapter = ArrayList<MembersCalendarHeader>()
                            data.companies?.let { companies ->

                                var companieslist = companies

                                companieslist = companieslist.filter {
                                    val listCategories: java.util.ArrayList<Integer> = java.util.ArrayList<Integer>()
                                    if (!filterMembers.categorysSelected.isEmpty()) {
                                        filterMembers.categorysSelected.forEach { itInt ->
                                            listCategories.add(itInt.id)
                                        }
                                        listCategories.containsAll(it.categories!!)
                                    } else {
                                        true
                                    }
                                }

                                val id = filterMembers.sortBy.id
                                when (id) {
                                    Integer(1) -> {

                                    }
                                    Integer(2) -> {
                                        companieslist = companieslist
                                                .sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER
                                                        ,{it.name}))
                                    }
                                    Integer(3) -> {
                                        companieslist = companieslist
                                                .sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER
                                                        ,{it.name})).reversed()
                                    }
                                    else -> {
                                    }
                                }


                                companieslist.forEach {company ->
                                    data.people?.let { people ->
                                        val peopleList = people.filter { it.company
                                                ?.id.equals(company.id) }


                                        listAdapter.add(MembersCalendarHeader(Gson()
                                                .toJson(CompaniesModel(company,peopleList))
                                                ,peopleList))

                                    }
                                }
                            }
                            membersCalendarHeaderList = listAdapter
                            setAdapter(membersCalendarHeaderList)
                        }
                    }
                })
    }

    private fun setAdapter(listAdapter: ArrayList<MembersCalendarHeader>) {
        val adapter = MembersAdapter(listAdapter) {
            val model = Gson().toJson(it)
            context?.let { it1 -> ActivityFragment.start(it1, PeopleProfile::class.java.name, model) }
        }
        list.adapter = adapter
    }

    private fun filter(text: String) {
        val filterdNames = ArrayList<MembersCalendarHeader>()
        for (p in membersCalendarHeaderList) {
            val peopleStr ="${p.title}".toLowerCase()
            if(peopleStr.indexOf(text.toLowerCase())>-1){
                filterdNames.add(p)
            }
        }
        setAdapter(filterdNames)
    }

    private val mBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val filterEvent: FilterMembers = Gson()
                    .fromJson(intent.getStringExtra("model"), FilterMembers::class.java)
            listsFilterEvent = filterEvent
            loadData(listsFilterEvent)
        }
    }

    override fun onStart() {
        super.onStart()
        activity?.registerReceiver(mBroadCastReceiver, IntentFilter(
                "filterMembers"))
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(mBroadCastReceiver)
    }
}