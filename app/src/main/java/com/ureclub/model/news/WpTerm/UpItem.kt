package com.ureclub.model.news.WpTerm

import android.os.Parcel
import android.os.Parcelable

data class UpItem(val href: String = "",
                  val embeddable: Boolean = false):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(href)
        parcel.writeByte(if (embeddable) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UpItem> {
        override fun createFromParcel(parcel: Parcel): UpItem {
            return UpItem(parcel)
        }

        override fun newArray(size: Int): Array<UpItem?> {
            return arrayOfNulls(size)
        }
    }
}