package com.ureclub.model

import android.os.Parcel
import android.os.Parcelable
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.model.event.DataItemEvent
import com.ureclub.model.news.ModelNews
import java.util.*
import kotlin.collections.ArrayList


public class NewsHeader (title: String, items: List<ModelNews>) :
        ExpandableGroup<ModelNews>(title, items)

public class MembersCalendarHeader (title: String, items: List<PeopleItem>) :
        ExpandableGroup<PeopleItem>(title, items)

