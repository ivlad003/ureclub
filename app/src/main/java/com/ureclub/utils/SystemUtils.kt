package com.ureclub.utils

import android.app.AlertDialog
import android.content.Context
import android.os.Build
import com.ureclub.R
import java.util.*




class SystemUtils{

    companion object {
        fun getLang():String{
            val lang = Locale.getDefault().language
            if(lang.isEmpty()){
                return "en"
            }
            return lang
        }

        fun showDialogBookingDecline(context: Context,di : DialogInterface){
            val builder: AlertDialog.Builder
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert)
            } else {
                builder = AlertDialog.Builder(context)
            }
            builder.setTitle(R.string.alert_booking_decline_title)
                    .setMessage(R.string.alert_booking_decline_message)
                    .setPositiveButton(android.R.string.yes, { dialog, which ->
                        di.positive()
                    })
                    .setNegativeButton(android.R.string.no, { dialog, which ->
                        dialog.dismiss()
                    })
                    .setIcon(R.mipmap.ic_app)
                    .show()
        }

        fun showDialogBookingSuccess(context: Context,di : DialogInterface){
            val builder: AlertDialog.Builder
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert)
            } else {
                builder = AlertDialog.Builder(context)
            }
            builder.setTitle(R.string.alert_booked_event_message)
                    .setMessage(R.string.alert_booking_success_message)
                    .setPositiveButton(android.R.string.yes, { dialog, which ->
                        di.positive()
                    })
                    .setNegativeButton(android.R.string.no, { dialog, which ->
                        dialog.dismiss()
                    })
                    .setIcon(R.mipmap.ic_app)
                    .show()
        }
    }


}

interface DialogInterface {
    fun positive()
}