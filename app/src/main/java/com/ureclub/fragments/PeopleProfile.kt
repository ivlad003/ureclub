package com.ureclub.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.ureclub.R
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.utils.CircleTransform
import kotlinx.android.synthetic.main.user_profile.*


@SuppressLint("ValidFragment")
class PeopleProfile : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.user_profile,
            container, false)

    companion object {
        fun newInstance(model: String, fragment: Fragment): PeopleProfile {
            val fragment = PeopleProfile()
            val args = Bundle()
            args.putSerializable("model", model)
            fragment.arguments = args
            fragment.setTargetFragment(fragment, 1)
            return fragment
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val people = Gson().fromJson(arguments?.getString("model"), PeopleItem::class.java)
        people?.let {
            name.text = "${it.firstName} ${it.lastName}"
            profession.text = it.position
            email.text = it.email
            val phoneStr = it.phone
            phone.text = ""
            if (!phoneStr.isNullOrEmpty() && !phoneStr.equals("hidden"))
                phone.text = phoneStr
            if (it.links?.image?.isNotEmpty()!!)
                Picasso.with(context).load(it.links.image)
                        .resize(300, 300)
                        .noFade()
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .placeholder(R.mipmap.user_profile)
                        .error(R.mipmap.user_profile).transform(CircleTransform()).into(icon)

            description.text = it.description
            name_company.text = it.company?.name
            progress.visibility = View.GONE
        }
        notif.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
        notif.visibility = View.GONE
        menu.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)

        menu.setOnClickListener {
            activity?.onBackPressed()
        }
        phone.setOnClickListener {
            tellToPhone(people.phone)
        }
        email.setOnClickListener {
            sendEmail(people.email)
        }


        val fb = people.links?.facebook


        val fbis = fb?.isEmpty()!!
        fb_bt.visibility = if(fb?.isEmpty()!!) View.INVISIBLE else View.VISIBLE

        fb_bt.setOnClickListener {

            if (fb?.isNotEmpty()!!) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(fb))
                startActivity(intent)
            }
        }

        val In = people.links?.linkedIn

        val inis = In?.isEmpty()!!
        lin_bt.visibility = if(In?.isEmpty()!!) View.INVISIBLE else View.VISIBLE

        lin_bt.setOnClickListener {

            if (In?.isNotEmpty()!!) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(In))
                startActivity(intent)
            }
        }

    }


    private fun tellToPhone(number: String?) {
        if (number?.isNotEmpty()!!) {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null))
            startActivity(intent)
        }
    }

    private fun sendEmail(email: String) {
        val uri = Uri.parse("mailto:" + email)
                .buildUpon()
                .build()

        val emailIntent = Intent(Intent.ACTION_SENDTO, uri)
        startActivity(Intent.createChooser(emailIntent, "Email send"))
    }
}