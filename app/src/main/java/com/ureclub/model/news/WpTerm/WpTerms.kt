package com.ureclub.model.news.WpTerm

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class WpTerms(@SerializedName("_links")
                   val Links: Links?,
                   val link: String = "",
                   val name: String = "",
                   val id: Int = 0,
                   val taxonomy: String = "",
                   val slug: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(com.ureclub.model.news.WpTerm.Links::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(Links, flags)
        parcel.writeString(link)
        parcel.writeString(name)
        parcel.writeInt(id)
        parcel.writeString(taxonomy)
        parcel.writeString(slug)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WpTerms> {
        override fun createFromParcel(parcel: Parcel): WpTerms {
            return WpTerms(parcel)
        }

        override fun newArray(size: Int): Array<WpTerms?> {
            return arrayOfNulls(size)
        }
    }
}