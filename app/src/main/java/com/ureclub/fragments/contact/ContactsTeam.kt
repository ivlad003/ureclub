package com.ureclub.fragments.contact

import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.activitys.Main
import com.ureclub.adapters.ContactsAdapter
import com.ureclub.extention.inTransaction
import com.ureclub.fragments.PeopleProfile
import com.ureclub.model.contacts.Contacts
import com.ureclub.model.contacts.ContactsModel
import kotlinx.android.synthetic.main.contacts.*
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactsTeam : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.contacts,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView.layoutManager = LinearLayoutManager(context)
        progress.visibility = View.VISIBLE
        val token = "${App.profile.token}"
        App.Companion.api.allContacts(token)
                .enqueue(object : Callback<ContactsModel> {
                    override fun onFailure(call: Call<ContactsModel>?, t: Throwable?) {
                        progress.visibility = View.GONE
                    }

                    override fun onResponse(call: Call<ContactsModel>?, response: Response<ContactsModel>?) {
                        progress.visibility = View.GONE
                        val model = response?.body()
                        model?.data?.let {
                            it.people?.let {


                                var list = it.filter { it.type.equals("worker") }
                                list = list.sortedWith(compareBy({ it.priority}))

                                recyclerView.adapter = ContactsAdapter(list) {
                                    val model = Gson().toJson(it)
                                    context?.let { it1 -> ActivityFragment.start(it1, PeopleProfile::class.java.name, model) }
                                }
                            }
                        }
                    }
                })
    }
}

