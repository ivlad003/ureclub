package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ModelNews(val date: String = "",
                     val template: String = "",
                     @SerializedName("_links")
                     val Links: Links?,
                     val link: String = "",
                     val type: String = "",
                     val title: Title?,
                     val content: Content?,
                     @SerializedName("featured_media")
                     val featuredMedia: Int = 0,
                     @SerializedName("_embedded")
                     val Embedded: Embedded?,
                     val modified: String = "",
                     val id: Int = 0,
                     val categories: List<Integer>?,
                     @SerializedName("date_gmt")
                     val dateGmt: String = "",
                     val slug: String = "",
                     @SerializedName("modified_gmt")
                     val modifiedGmt: String = "",
                     val author: Int = 0,
                     val format: String = "",
                     @SerializedName("comment_status")
                     val commentStatus: String = "",
                     @SerializedName("ping_status")
                     val pingStatus: String = "",
                     val sticky: Boolean = false,
                     val guid: Guid?,
                     val excerpt: Excerpt?,
                     val status: String = ""): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(com.ureclub.model.news.Links::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Title::class.java.classLoader),
            TODO("content"),
            parcel.readInt(),
            TODO("Embedded"),
            parcel.readString(),
            parcel.readInt(),
            TODO("categories"),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            TODO("guid"),
            parcel.readParcelable(Excerpt::class.java.classLoader),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
        parcel.writeString(template)
        parcel.writeParcelable(Links, flags)
        parcel.writeString(link)
        parcel.writeString(type)
        parcel.writeParcelable(title, flags)
        parcel.writeInt(featuredMedia)
        parcel.writeString(modified)
        parcel.writeInt(id)
        parcel.writeString(dateGmt)
        parcel.writeString(slug)
        parcel.writeString(modifiedGmt)
        parcel.writeInt(author)
        parcel.writeString(format)
        parcel.writeString(commentStatus)
        parcel.writeString(pingStatus)
        parcel.writeByte(if (sticky) 1 else 0)
        parcel.writeParcelable(excerpt, flags)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelNews> {
        override fun createFromParcel(parcel: Parcel): ModelNews {
            return ModelNews(parcel)
        }

        override fun newArray(size: Int): Array<ModelNews?> {
            return arrayOfNulls(size)
        }
    }
}