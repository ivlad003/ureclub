package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable

data class WpAttachmentItem(val href: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(href)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WpAttachmentItem> {
        override fun createFromParcel(parcel: Parcel): WpAttachmentItem {
            return WpAttachmentItem(parcel)
        }

        override fun newArray(size: Int): Array<WpAttachmentItem?> {
            return arrayOfNulls(size)
        }
    }
}