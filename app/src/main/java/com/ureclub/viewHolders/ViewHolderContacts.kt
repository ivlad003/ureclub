package com.ureclub.viewHolders
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.ureclub.App
import com.ureclub.R
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.utils.CircleTransform
import kotlinx.android.synthetic.main.contacts_item.view.*

class ViewHolderContacts(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: PeopleItem, listener: (PeopleItem) -> Unit) = with(itemView) {
        title.text =  "${item.firstName} ${item.lastName}"
        subTitle.text = item.position
        email.text = item.email
        phone.text = item.phone
        val indL = item.links?.linkedIn
        val fbL = item.links?.facebook
        if (indL?.isEmpty()!!) {
            lin.visibility = View.GONE
        }else{
            lin.visibility = View.VISIBLE
        }

        if (fbL?.isEmpty()!!) {
            fb.visibility = View.GONE
        }else{
            fb.visibility = View.VISIBLE
        }

        fb.setOnClickListener {
            if (fbL.isNotEmpty()) {
                openActionIntent(fbL)
            }
        }
        lin.setOnClickListener {

            if (indL.isNotEmpty()) {
                openActionIntent(indL)
            }
        }

        Picasso.with(context).load(item.links.image)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .error(R.mipmap.user_profile)
                .placeholder(R.mipmap.user_profile)
                .transform(CircleTransform()).into(icon)

        setOnClickListener { listener(item) }
    }

    private fun openActionIntent(ind: String?) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(ind))
        itemView.context.startActivity(intent)
    }
}