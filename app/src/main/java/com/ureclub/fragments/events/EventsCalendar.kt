package com.ureclub.fragments.events


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.adapters.EventCalendarAdapter
import com.ureclub.model.FilterEvent
import com.ureclub.model.UpcomingPast
import com.ureclub.model.event.DataItemEvent
import com.ureclub.model.event.EventModel
import com.ureclub.utils.SystemUtils
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.event_calendar.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*


class EventsCalendar : Fragment() {



    var listsFilterEvent= FilterEvent(ArrayList(), ArrayList())

    var listSort: UpcomingPast = UpcomingPast.upcoming

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.event_calendar,
            container, false)

    val sectionAdapter = SectionedRecyclerViewAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadData(listsFilterEvent)
        eventCalendarRecyclerView.layoutManager = LinearLayoutManager(context) as RecyclerView.LayoutManager?

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            when (i) {
                R.id.radio_upcoming -> {
                    listSort = UpcomingPast.upcoming
                    sectionAdapter.removeAllSections()
                    sectionAdapter.notifyDataSetChanged()
                    loadData(listsFilterEvent)
                }
                else -> {
                    listSort = UpcomingPast.past
                    sectionAdapter.removeAllSections()
                    sectionAdapter.notifyDataSetChanged()
                    loadData(listsFilterEvent)
                }
            }
        }

        tryAgain.setOnClickListener {
            loadData(listsFilterEvent)
        }
    }


    private fun loadData(event: FilterEvent?) {
        containerInfo.visibility = View.GONE
        progress.visibility = View.VISIBLE
        val token = "${App.profile.token}"
        App.Companion.api.allEvents(token, SystemUtils.getLang()).enqueue(object : Callback<EventModel> {
            override fun onFailure(call: Call<EventModel>?, t: Throwable?) {
                containerInfo.visibility = View.VISIBLE
                progress.visibility = View.GONE
                infoImage.setBackgroundResource(R.drawable.ic_server_error)
            }

            override fun onResponse(call: Call<EventModel>?, response: Response<EventModel>?) {
                containerInfo.visibility = View.GONE
                progress.visibility = View.GONE

                val model = response?.body()
                model?.data?.let {

                    val dfs = DateFormatSymbols()
                    val months = dfs.getMonths()

                    var itlist = it



                    event?.let {
                        itlist = itlist.filter {

                            if (!event.yersSelected.isEmpty()) {
                                val yers = it.date?.dateBeg?.split("-")?.get(0)
                                val list = ArrayList<String>()
                                event.yersSelected.forEach {
                                    list.add(it.name)
                                }

                                Log.d("onResponse","list.contains(yers) "+list.contains(yers))
                                Log.d("onResponse","it.date?.dateBeg "+it.date?.dateBeg)
                                Log.d("onResponse","event.yersSelected "+event.yersSelected)
                                list.contains(yers)
                            } else {
                                true
                            }
                        }


                        itlist = itlist.filter<DataItemEvent> {
                            val listCategories: ArrayList<Integer> = ArrayList<Integer>()
                            if (!event.categorysSelected.isEmpty()) {
                                event.categorysSelected.forEach { itInt ->
                                    listCategories.add(itInt.id)
                                }

                                val categories: ArrayList<Integer> = ArrayList<Integer>()
                                it.categories?.let {
                                    it.forEach {
                                        categories.add(it)
                                    }
                                }

                                categories.containsAll(listCategories)
                            } else {
                                true
                            }
                        }

                        itlist = itlist.filter {
                            val date = SimpleDateFormat("yyyy-MM-dd").parse(it.date?.dateBeg)

                            if(listSort == UpcomingPast.upcoming){
                                Date().before(date)
                            }else{
                                Date().after(date)
                            }
                        }

                    }


                    if(listSort == UpcomingPast.past){
                        itlist = itlist.sortedWith(compareBy({ it.date?.dateBeg })).reversed()
                    }else{
                        itlist = itlist.sortedWith(compareBy({ it.date?.dateBeg }))
                    }

                    var list = itlist.groupBy { item ->
                        val m = item.date?.dateBeg?.split("-")?.get(1)
                        val y = item.date?.dateBeg?.split("-")?.get(0)
                        "${m}-${y}"
                    }




                    list.forEach { (k, value) ->
                        val m = k.split("-")[0].replace("0", "")
                        val y = k.split("-")[1]
                        val monthIndex = m.toInt() - 1
                        val section = EventCalendarAdapter("${y} ${months[monthIndex]}", value) {
                            val model = Gson().toJson(it)
                            context?.let { it1 -> ActivityFragment.start(it1, EventAbout::class.java.name, model) }
                        }
                        sectionAdapter.addSection(section)
                    }




                    eventCalendarRecyclerView.setHasFixedSize(true)
                    eventCalendarRecyclerView.adapter = sectionAdapter
                }
            }

        })
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: FilterEvent) {
        listsFilterEvent = event
        loadData(event)
    }

    private val mBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            sectionAdapter.removeAllSections()
            sectionAdapter.notifyDataSetChanged()
            val filterEvent: FilterEvent = Gson()
                    .fromJson(intent.getStringExtra("model"), FilterEvent::class.java)
            listsFilterEvent = filterEvent
            loadData(filterEvent)
        }
    }

    override fun onStart() {
        super.onStart()
        activity?.registerReceiver(mBroadCastReceiver, IntentFilter(
                "filter"))
    }

    override fun onStop() {
        super.onStop()

    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(mBroadCastReceiver)
    }
}