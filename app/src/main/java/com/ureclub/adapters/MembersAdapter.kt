package com.ureclub.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.ureclub.R
import com.ureclub.model.NewsModel
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.model.news.ModelNews
import com.ureclub.viewHolders.ViewHolderHeaderCompany
import com.ureclub.viewHolders.ViewHolderHeaderNews
import com.ureclub.viewHolders.ViewHolderMember
import com.ureclub.viewHolders.ViewHolderNews

class MembersAdapter(groups: List<ExpandableGroup<*>>, private val listener: (PeopleItem) -> Unit)
    : ExpandableRecyclerViewAdapter<ViewHolderHeaderCompany, ViewHolderMember>(groups) {
    override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): ViewHolderHeaderCompany {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.companie_item_header, parent, false)
        return ViewHolderHeaderCompany(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMember {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.member_item, parent, false)
        return ViewHolderMember(view)
    }

    override fun onBindChildViewHolder(holder: ViewHolderMember, flatPosition: Int, group: ExpandableGroup<*>,
                                       childIndex: Int) {
        val item =  group.items.get(childIndex) as PeopleItem
        holder.bind(item,listener)
    }

    override fun onBindGroupViewHolder(holder: ViewHolderHeaderCompany, flatPosition: Int,
                                       group: ExpandableGroup<*>) {
        holder.bind(group)
    }
}