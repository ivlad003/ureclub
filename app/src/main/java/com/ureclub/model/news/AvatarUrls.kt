package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class AvatarUrls(@SerializedName("24")
                      val I24: String = "",
                      @SerializedName("48")
                      val I48: String = "",
                      @SerializedName("96")
                      val I96: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(I24)
        parcel.writeString(I48)
        parcel.writeString(I96)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AvatarUrls> {
        override fun createFromParcel(parcel: Parcel): AvatarUrls {
            return AvatarUrls(parcel)
        }

        override fun newArray(size: Int): Array<AvatarUrls?> {
            return arrayOfNulls(size)
        }
    }
}