package com.ureclub.model.filter

data class Data(val events: Events,
                val contacts: Contacts)