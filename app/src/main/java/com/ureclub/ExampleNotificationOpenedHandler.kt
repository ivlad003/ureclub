package com.ureclub

import android.content.Context
import android.content.Intent
import android.util.Log
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal
import com.ureclub.activitys.Main


class ExampleNotificationOpenedHandler : OneSignal.NotificationOpenedHandler {
    override fun notificationOpened(result: OSNotificationOpenResult?) {
//         OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.WARN)
        val payload = result?.notification?.payload

        val postID = payload?.additionalData?.getString("postIDs")
                ?.replace("[","")
                ?.replace("]","")?.split(",")

        val postType = payload?.additionalData?.getString("postType")

        Log.d("Notification__2","Notification postID: ${postID} postType: ${postType}")
        val intent = Intent(App.instance, Main::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        postID?.let {
            if (!it.get(0).isNullOrEmpty()) {
                intent.putExtra("postID", it.get(0).toInt())
                intent.putExtra("postType", postType)
            }
        }
        Log.d("Notification__2","Notification App.instance: ${App.instance}")
        App.instance.startActivity(intent)
    }
}