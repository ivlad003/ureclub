package com.ureclub.viewHolders

import android.view.View
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kotlinx.android.synthetic.main.header_item_events_list.view.*


class ViewHolderHeaderCalendarEvent(itemView: View) : GroupViewHolder(itemView) {
    fun bind(group: String) = with(itemView) {
        headTitle.text = group
    }
}