package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class AuthorItem(@SerializedName("avatar_urls")
                      val avatarUrls: AvatarUrls?,
                      @SerializedName("_links")
                      val Links: Links?,
                      val name: String = "",
                      val link: String = "",
                      val description: String = "",
                      val id: Int = 0,
                      val url: String = "",
                      val slug: String = ""): Parcelable {
    constructor(parcel: Parcel) : this(
            TODO("avatarUrls"),
            TODO("Links"),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(link)
        parcel.writeString(description)
        parcel.writeInt(id)
        parcel.writeString(url)
        parcel.writeString(slug)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AuthorItem> {
        override fun createFromParcel(parcel: Parcel): AuthorItem {
            return AuthorItem(parcel)
        }

        override fun newArray(size: Int): Array<AuthorItem?> {
            return arrayOfNulls(size)
        }
    }
}