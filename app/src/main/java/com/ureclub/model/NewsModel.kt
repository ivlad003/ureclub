package com.ureclub.model

import android.os.Parcel
import android.os.Parcelable


class NewsModel protected constructor(`in`: Parcel) : Parcelable {

    var id: Int = 0
    var date: String
    var date_gmt: String
    var guid: GuidBean? = null
    var modified: String
    var modified_gmt: String
    var slug: String
    var status: String
    var type: String
    var link: String
    var title: TitleBean? = null
    var content: ContentBean? = null
    var excerpt: ExcerptBean? = null
    var author: Int = 0
    var featured_media: Int = 0
    var comment_status: String
    var ping_status: String
    var sticky: Boolean = false
    var template: String
    var format: String
    var _links: LinksBean? = null
    var _embedded: EmbeddedBean? = null
    var meta: List<*>? = null
    var categories: List<Int>? = null
    var tags: List<*>? = null
    var acf: List<*>? = null

    init {
        id = `in`.readInt()
        date = `in`.readString()
        date_gmt = `in`.readString()
        modified = `in`.readString()
        modified_gmt = `in`.readString()
        slug = `in`.readString()
        status = `in`.readString()
        type = `in`.readString()
        link = `in`.readString()
        author = `in`.readInt()
        featured_media = `in`.readInt()
        comment_status = `in`.readString()
        ping_status = `in`.readString()
        sticky = `in`.readByte().toInt() != 0
        template = `in`.readString()
        format = `in`.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeInt(id)
        parcel.writeString(date)
        parcel.writeString(date_gmt)
        parcel.writeString(modified)
        parcel.writeString(modified_gmt)
        parcel.writeString(slug)
        parcel.writeString(status)
        parcel.writeString(type)
        parcel.writeString(link)
        parcel.writeInt(author)
        parcel.writeInt(featured_media)
        parcel.writeString(comment_status)
        parcel.writeString(ping_status)
        parcel.writeByte((if (sticky) 1 else 0).toByte())
        parcel.writeString(template)
        parcel.writeString(format)
    }

    class GuidBean protected constructor(`in`: Parcel) : Parcelable {
        var rendered: String

        init {
            rendered = `in`.readString()
        }

        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(parcel: Parcel, i: Int) {
            parcel.writeString(rendered)
        }

        companion object {

            val CREATOR: Parcelable.Creator<GuidBean> = object : Parcelable.Creator<GuidBean> {
                override fun createFromParcel(`in`: Parcel): GuidBean {
                    return GuidBean(`in`)
                }

                override fun newArray(size: Int): Array<GuidBean?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    class TitleBean protected constructor(`in`: Parcel) : Parcelable {
        var rendered: String

        init {
            rendered = `in`.readString()
        }

        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(parcel: Parcel, i: Int) {
            parcel.writeString(rendered)
        }

        companion object {

            val CREATOR: Parcelable.Creator<TitleBean> = object : Parcelable.Creator<TitleBean> {
                override fun createFromParcel(`in`: Parcel): TitleBean {
                    return TitleBean(`in`)
                }

                override fun newArray(size: Int): Array<TitleBean?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    class ContentBean protected constructor(`in`: Parcel) : Parcelable {
        var rendered: String
        var protectedX: Boolean = false

        init {
            rendered = `in`.readString()
            protectedX = `in`.readByte().toInt() != 0
        }

        override fun writeToParcel(dest: Parcel, flags: Int) {
            dest.writeString(rendered)
            dest.writeByte((if (protectedX) 1 else 0).toByte())
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object {

            val CREATOR: Parcelable.Creator<ContentBean> = object : Parcelable.Creator<ContentBean> {
                override fun createFromParcel(`in`: Parcel): ContentBean {
                    return ContentBean(`in`)
                }

                override fun newArray(size: Int): Array<ContentBean?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    class ExcerptBean protected constructor(`in`: Parcel) : Parcelable {
        var rendered: String
        var protectedX: Boolean = false

        init {
            rendered = `in`.readString()
            protectedX = `in`.readByte().toInt() != 0
        }

        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(parcel: Parcel, i: Int) {
            parcel.writeString(rendered)
            parcel.writeByte((if (protectedX) 1 else 0).toByte())
        }

        companion object {

            val CREATOR: Parcelable.Creator<ExcerptBean> = object : Parcelable.Creator<ExcerptBean> {
                override fun createFromParcel(`in`: Parcel): ExcerptBean {
                    return ExcerptBean(`in`)
                }

                override fun newArray(size: Int): Array<ExcerptBean?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    // FIXME generate failure  field _$WpTerm116
    // FIXME generate failure  field _$WpAttachment246
    class LinksBean protected constructor(@SuppressWarnings("UnusedParameters") parcel: Parcel) : Parcelable {
        var self: List<SelfBean>? = null
        var collection: List<CollectionBean>? = null
        var about: List<AboutBean>? = null
        var author: List<AuthorBean>? = null
        var replies: List<RepliesBean>? = null
        var versionhistory: List<VersionhistoryBean>? = null
        var curies: List<CuriesBean>? = null

        class SelfBean protected constructor(`in`: Parcel) : Parcelable {
            var href: String

            init {
                href = `in`.readString()
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeString(href)
            }

            override fun describeContents(): Int {
                return 0
            }

            companion object {

                val CREATOR: Parcelable.Creator<SelfBean> = object : Parcelable.Creator<SelfBean> {
                    override fun createFromParcel(`in`: Parcel): SelfBean {
                        return SelfBean(`in`)
                    }

                    override fun newArray(size: Int): Array<SelfBean?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        class CollectionBean protected constructor(`in`: Parcel) : Parcelable {
            var href: String

            init {
                href = `in`.readString()
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeString(href)
            }

            override fun describeContents(): Int {
                return 0
            }

            companion object {

                val CREATOR: Parcelable.Creator<CollectionBean> = object : Parcelable.Creator<CollectionBean> {
                    override fun createFromParcel(`in`: Parcel): CollectionBean {
                        return CollectionBean(`in`)
                    }

                    override fun newArray(size: Int): Array<CollectionBean?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        class AboutBean protected constructor(`in`: Parcel) : Parcelable {
            var href: String

            init {
                href = `in`.readString()
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeString(href)
            }

            override fun describeContents(): Int {
                return 0
            }

            companion object {

                val CREATOR: Parcelable.Creator<AboutBean> = object : Parcelable.Creator<AboutBean> {
                    override fun createFromParcel(`in`: Parcel): AboutBean {
                        return AboutBean(`in`)
                    }

                    override fun newArray(size: Int): Array<AboutBean?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        class AuthorBean protected constructor(`in`: Parcel) : Parcelable {
            var embeddable: Boolean = false
            var href: String

            init {
                embeddable = `in`.readByte().toInt() != 0
                href = `in`.readString()
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeByte((if (embeddable) 1 else 0).toByte())
                dest.writeString(href)
            }

            override fun describeContents(): Int {
                return 0
            }

            companion object {

                val CREATOR: Parcelable.Creator<AuthorBean> = object : Parcelable.Creator<AuthorBean> {
                    override fun createFromParcel(`in`: Parcel): AuthorBean {
                        return AuthorBean(`in`)
                    }

                    override fun newArray(size: Int): Array<AuthorBean?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        class RepliesBean protected constructor(`in`: Parcel) : Parcelable {
            var embeddable: Boolean = false
            var href: String

            init {
                embeddable = `in`.readByte().toInt() != 0
                href = `in`.readString()
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeByte((if (embeddable) 1 else 0).toByte())
                dest.writeString(href)
            }

            override fun describeContents(): Int {
                return 0
            }

            companion object {

                val CREATOR: Parcelable.Creator<RepliesBean> = object : Parcelable.Creator<RepliesBean> {
                    override fun createFromParcel(`in`: Parcel): RepliesBean {
                        return RepliesBean(`in`)
                    }

                    override fun newArray(size: Int): Array<RepliesBean?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        class VersionhistoryBean protected constructor(`in`: Parcel) : Parcelable {
            var href: String

            init {
                href = `in`.readString()
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                dest.writeString(href)
            }

            override fun describeContents(): Int {
                return 0
            }

            companion object {

                val CREATOR: Parcelable.Creator<VersionhistoryBean> = object : Parcelable.Creator<VersionhistoryBean> {
                    override fun createFromParcel(`in`: Parcel): VersionhistoryBean {
                        return VersionhistoryBean(`in`)
                    }

                    override fun newArray(size: Int): Array<VersionhistoryBean?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        class Wp protected constructor(`in`: Parcel) : Parcelable {
            var taxonomy: String
            var embeddable: Boolean = false
            var href: String

            init {
                taxonomy = `in`.readString()
                embeddable = `in`.readByte().toInt() != 0
                href = `in`.readString()
            }

            override fun describeContents(): Int {
                return 0
            }

            override fun writeToParcel(parcel: Parcel, i: Int) {
                parcel.writeString(taxonomy)
                parcel.writeByte((if (embeddable) 1 else 0).toByte())
                parcel.writeString(href)
            }

            companion object {

                val CREATOR: Parcelable.Creator<Wp> = object : Parcelable.Creator<Wp> {
                    override fun createFromParcel(`in`: Parcel): Wp {
                        return Wp(`in`)
                    }

                    override fun newArray(size: Int): Array<Wp?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        class CuriesBean protected constructor(`in`: Parcel) : Parcelable {
            var name: String
            var href: String
            var templated: Boolean = false

            init {
                name = `in`.readString()
                href = `in`.readString()
                templated = `in`.readByte().toInt() != 0
            }

            override fun describeContents(): Int {
                return 0
            }

            override fun writeToParcel(parcel: Parcel, i: Int) {
                parcel.writeString(name)
                parcel.writeString(href)
                parcel.writeByte((if (templated) 1 else 0).toByte())
            }

            companion object {

                val CREATOR: Parcelable.Creator<CuriesBean> = object : Parcelable.Creator<CuriesBean> {
                    override fun createFromParcel(`in`: Parcel): CuriesBean {
                        return CuriesBean(`in`)
                    }

                    override fun newArray(size: Int): Array<CuriesBean?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        override fun writeToParcel(dest: Parcel, flags: Int) {}

        override fun describeContents(): Int {
            return 0
        }

        companion object {

            val CREATOR: Parcelable.Creator<LinksBean> = object : Parcelable.Creator<LinksBean> {
                override fun createFromParcel(p: Parcel): LinksBean {
                    return LinksBean(p)
                }

                override fun newArray(size: Int): Array<LinksBean?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    // FIXME generate failure  field _$WpTerm191
    class EmbeddedBean protected constructor(`in`: Parcel) : Parcelable {
        var author: List<AuthorBeanX>? = null

        var id: Int = 0
        var link: String
        var name: String
        var slug: String
        var taxonomy: String
        var _links: LinksBeanXX? = null
        var acf: List<*>? = null

        init {
            id = `in`.readInt()
            link = `in`.readString()
            name = `in`.readString()
            slug = `in`.readString()
            taxonomy = `in`.readString()
        }

        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(parcel: Parcel, i: Int) {
            parcel.writeInt(id)
            parcel.writeString(link)
            parcel.writeString(name)
            parcel.writeString(slug)
            parcel.writeString(taxonomy)
        }

        class AuthorBeanX protected constructor(`in`: Parcel) : Parcelable {
            var id: Int = 0
            var name: String
            var url: String
            var description: String
            var link: String
            var slug: String
            var avatar_urls: AvatarUrlsBean? = null
            var _links: LinksBeanX? = null
            var acf: List<*>? = null

            init {
                id = `in`.readInt()
                name = `in`.readString()
                url = `in`.readString()
                description = `in`.readString()
                link = `in`.readString()
                slug = `in`.readString()
            }

            override fun describeContents(): Int {
                return 0
            }

            override fun writeToParcel(parcel: Parcel, i: Int) {
                parcel.writeInt(id)
                parcel.writeString(name)
                parcel.writeString(url)
                parcel.writeString(description)
                parcel.writeString(link)
                parcel.writeString(slug)
            }

            class AvatarUrlsBean protected constructor(`in`: Parcel) : Parcelable {
                var `_$24`: String
                var `_$48`: String
                var `_$96`: String

                init {
                    `_$24` = `in`.readString()
                    `_$48` = `in`.readString()
                    `_$96` = `in`.readString()
                }

                override fun describeContents(): Int {
                    return 0
                }

                override fun writeToParcel(parcel: Parcel, i: Int) {
                    parcel.writeString(`_$24`)
                    parcel.writeString(`_$48`)
                    parcel.writeString(`_$96`)
                }

                companion object {

                    val CREATOR: Parcelable.Creator<AvatarUrlsBean> = object : Parcelable.Creator<AvatarUrlsBean> {
                        override fun createFromParcel(`in`: Parcel): AvatarUrlsBean {
                            return AvatarUrlsBean(`in`)
                        }

                        override fun newArray(size: Int): Array<AvatarUrlsBean?> {
                            return arrayOfNulls(size)
                        }
                    }
                }
            }

            class LinksBeanX protected constructor(@SuppressWarnings("UnusedParameters") parcel: Parcel) : Parcelable {
                var self: List<SelfBeanX>? = null
                var collection: List<CollectionBeanX>? = null

                override fun describeContents(): Int {
                    return 0
                }

                override fun writeToParcel(parcel: Parcel, i: Int) {}

                class SelfBeanX protected constructor(`in`: Parcel) : Parcelable {
                    var href: String

                    init {
                        href = `in`.readString()
                    }

                    override fun describeContents(): Int {
                        return 0
                    }

                    override fun writeToParcel(parcel: Parcel, i: Int) {
                        parcel.writeString(href)
                    }

                    companion object {

                        val CREATOR: Parcelable.Creator<SelfBeanX> = object : Parcelable.Creator<SelfBeanX> {
                            override fun createFromParcel(`in`: Parcel): SelfBeanX {
                                return SelfBeanX(`in`)
                            }

                            override fun newArray(size: Int): Array<SelfBeanX?> {
                                return arrayOfNulls(size)
                            }
                        }
                    }
                }

                class CollectionBeanX protected constructor(`in`: Parcel) : Parcelable {
                    var href: String

                    init {
                        href = `in`.readString()
                    }

                    override fun describeContents(): Int {
                        return 0
                    }

                    override fun writeToParcel(parcel: Parcel, i: Int) {
                        parcel.writeString(href)
                    }

                    companion object {

                        val CREATOR: Parcelable.Creator<CollectionBeanX> = object : Parcelable.Creator<CollectionBeanX> {
                            override fun createFromParcel(`in`: Parcel): CollectionBeanX {
                                return CollectionBeanX(`in`)
                            }

                            override fun newArray(size: Int): Array<CollectionBeanX?> {
                                return arrayOfNulls(size)
                            }
                        }
                    }
                }

                companion object {

                    val CREATOR: Parcelable.Creator<LinksBeanX> = object : Parcelable.Creator<LinksBeanX> {
                        override fun createFromParcel(`in`: Parcel): LinksBeanX {
                            return LinksBeanX(`in`)
                        }

                        override fun newArray(size: Int): Array<LinksBeanX?> {
                            return arrayOfNulls(size)
                        }
                    }
                }
            }

            companion object {

                val CREATOR: Parcelable.Creator<AuthorBeanX> = object : Parcelable.Creator<AuthorBeanX> {
                    override fun createFromParcel(p: Parcel): AuthorBeanX {
                        return AuthorBeanX(p)
                    }

                    override fun newArray(size: Int): Array<AuthorBeanX?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }


        class LinksBeanXX protected constructor(@SuppressWarnings("UnusedParameters") parcel: Parcel) : Parcelable {
            var self: List<SelfBeanXX>? = null
            var collection: List<CollectionBeanXX>? = null
            var about: List<AboutBeanX>? = null
            var curies: List<CuriesBeanX>? = null

            override fun describeContents(): Int {
                return 0
            }

            override fun writeToParcel(parcel: Parcel, i: Int) {}

            class SelfBeanXX protected constructor(`in`: Parcel) : Parcelable {
                var href: String

                init {
                    href = `in`.readString()
                }

                override fun describeContents(): Int {
                    return 0
                }

                override fun writeToParcel(parcel: Parcel, i: Int) {
                    parcel.writeString(href)
                }

                companion object {

                    val CREATOR: Parcelable.Creator<SelfBeanXX> = object : Parcelable.Creator<SelfBeanXX> {
                        override fun createFromParcel(`in`: Parcel): SelfBeanXX {
                            return SelfBeanXX(`in`)
                        }

                        override fun newArray(size: Int): Array<SelfBeanXX?> {
                            return arrayOfNulls(size)
                        }
                    }
                }
            }

            class CollectionBeanXX protected constructor(`in`: Parcel) : Parcelable {
                var href: String

                init {
                    href = `in`.readString()
                }

                override fun describeContents(): Int {
                    return 0
                }

                override fun writeToParcel(parcel: Parcel, i: Int) {
                    parcel.writeString(href)
                }

                companion object {

                    val CREATOR: Parcelable.Creator<CollectionBeanXX> = object : Parcelable.Creator<CollectionBeanXX> {
                        override fun createFromParcel(`in`: Parcel): CollectionBeanXX {
                            return CollectionBeanXX(`in`)
                        }

                        override fun newArray(size: Int): Array<CollectionBeanXX?> {
                            return arrayOfNulls(size)
                        }
                    }
                }
            }

            class AboutBeanX protected constructor(`in`: Parcel) : Parcelable {
                var href: String

                init {
                    href = `in`.readString()
                }

                override fun describeContents(): Int {
                    return 0
                }

                override fun writeToParcel(parcel: Parcel, i: Int) {
                    parcel.writeString(href)
                }

                companion object {

                    val CREATOR: Parcelable.Creator<AboutBeanX> = object : Parcelable.Creator<AboutBeanX> {
                        override fun createFromParcel(`in`: Parcel): AboutBeanX {
                            return AboutBeanX(`in`)
                        }

                        override fun newArray(size: Int): Array<AboutBeanX?> {
                            return arrayOfNulls(size)
                        }
                    }
                }
            }


            class CuriesBeanX protected constructor(`in`: Parcel) : Parcelable {
                var name: String
                var href: String
                var templated: Boolean = false

                init {
                    name = `in`.readString()
                    href = `in`.readString()
                    templated = `in`.readByte().toInt() != 0
                }

                override fun writeToParcel(dest: Parcel, flags: Int) {
                    dest.writeString(name)
                    dest.writeString(href)
                    dest.writeByte((if (templated) 1 else 0).toByte())
                }

                override fun describeContents(): Int {
                    return 0
                }

                companion object {

                    val CREATOR: Parcelable.Creator<CuriesBeanX> = object : Parcelable.Creator<CuriesBeanX> {
                        override fun createFromParcel(`in`: Parcel): CuriesBeanX {
                            return CuriesBeanX(`in`)
                        }

                        override fun newArray(size: Int): Array<CuriesBeanX?> {
                            return arrayOfNulls(size)
                        }
                    }
                }
            }

            companion object {

                val CREATOR: Parcelable.Creator<LinksBeanXX> = object : Parcelable.Creator<LinksBeanXX> {
                    override fun createFromParcel(`in`: Parcel): LinksBeanXX {
                        return LinksBeanXX(`in`)
                    }

                    override fun newArray(size: Int): Array<LinksBeanXX?> {
                        return arrayOfNulls(size)
                    }
                }
            }
        }

        companion object {

            val CREATOR: Parcelable.Creator<EmbeddedBean> = object : Parcelable.Creator<EmbeddedBean> {
                override fun createFromParcel(`in`: Parcel): EmbeddedBean {
                    return EmbeddedBean(`in`)
                }

                override fun newArray(size: Int): Array<EmbeddedBean?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    companion object {

        val CREATOR: Parcelable.Creator<NewsModel> = object : Parcelable.Creator<NewsModel> {
            override fun createFromParcel(`in`: Parcel): NewsModel {
                return NewsModel(`in`)
            }

            override fun newArray(size: Int): Array<NewsModel?> {
                return arrayOfNulls(size)
            }
        }
    }
}
