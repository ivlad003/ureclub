package com.ureclub.fragments.contact

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ureclub.R
import com.ureclub.activitys.Main
import com.ureclub.extention.inTransaction
import kotlinx.android.synthetic.main.contacts_main.*
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*


class Contacts : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.contacts_main,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        toolbar_title.text = getString(R.string.menu_item_contacts)
        leftButton.setOnClickListener {
            (activity as Main).drawer.openDrawer(Gravity.START)
        }

        activity?.supportFragmentManager?.inTransaction {
            add(R.id.contactFrame, ContactsTeam())
        }

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            when (i) {
                R.id.radio_team -> {
                    activity?.supportFragmentManager?.inTransaction {
                        add(R.id.contactFrame, ContactsTeam())
                    }
                }
                else -> {
                    activity?.supportFragmentManager?.inTransaction {
                        add(R.id.contactFrame, ContactOffice())
                    }
                }
            }
        }
    }
}