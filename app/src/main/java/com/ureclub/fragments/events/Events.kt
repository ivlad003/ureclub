package com.ureclub.fragments.events

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.activitys.Main
import com.ureclub.extention.inTransaction
import com.ureclub.fragments.EventsFilters
import kotlinx.android.synthetic.main.events.*
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.toolbar.*

class Events : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.events,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        title.text = getString(R.string.menu_item_events)
        leftButton.setOnClickListener {
            (activity as Main).drawer.openDrawer(Gravity.START)
        }
        rightButton.setImageResource(R.mipmap.ic_filter)
        context?.let { rightButton.setColorFilter(ContextCompat.getColor(it, android.R.color.white)) }


        rightButton.setOnClickListener {
            context?.let { it1 -> ActivityFragment.start(it1, EventsFilters::class.java.name, "") }
        }

        arguments?.let {
            val frm = EventsList()
            val arg = Bundle()
            arg.putInt("id", it.getInt("id"))
            frm.arguments = arg
            activity?.supportFragmentManager?.inTransaction {
                add(R.id.eventFrame, frm)
            }
        }

        if (arguments == null)
            activity?.supportFragmentManager?.inTransaction {
                add(R.id.eventFrame, EventsList())
            }

        radioButton.setOnCheckedChangeListener { radioGroup, i ->
            when (i) {
                R.id.radio_calendar -> {
                    activity?.supportFragmentManager?.inTransaction {
                        add(R.id.eventFrame, EventsCalendar())
                    }
                }
                else -> {
                    activity?.supportFragmentManager?.inTransaction {
                        add(R.id.eventFrame, EventsList())
                    }
                }
            }
        }
    }
}