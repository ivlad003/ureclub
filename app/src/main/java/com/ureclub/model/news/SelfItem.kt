package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable

data class SelfItem(val href: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(href)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SelfItem> {
        override fun createFromParcel(parcel: Parcel): SelfItem {
            return SelfItem(parcel)
        }

        override fun newArray(size: Int): Array<SelfItem?> {
            return arrayOfNulls(size)
        }
    }
}