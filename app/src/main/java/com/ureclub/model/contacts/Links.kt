package com.ureclub.model.contacts

import android.os.Parcel
import android.os.Parcelable

data class Links(val image: String? = "",
                 val facebook: String = "",
                 val linkedIn: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
        parcel.writeString(facebook)
        parcel.writeString(linkedIn)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Links(image=$image, facebook='$facebook', linkedIn='$linkedIn')"
    }

    companion object CREATOR : Parcelable.Creator<Links> {
        override fun createFromParcel(parcel: Parcel): Links {
            return Links(parcel)
        }

        override fun newArray(size: Int): Array<Links?> {
            return arrayOfNulls(size)
        }
    }
}