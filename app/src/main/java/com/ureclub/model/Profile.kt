package com.ureclub.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class ProfileModel(var code: String, var data: Profile)

@Entity(tableName = "profile")
data class Profile(
        @SerializedName("linkedIn_link")
        var linkedInLink: String? = "",
        @SerializedName("image_link")
        var imageLink: String? = "",
        @SerializedName("company_id")
        var companyId: String? = "",
        @SerializedName("job_position")
        var jobPosition: String? = "",
        @SerializedName("facebook_link")
        var facebookLink: String? = "",
        @SerializedName("last_name")
        var lastName: String? = "",
        var description: String? = "",
        var nicename: String? = "",
        var token: String? = "",
        @Embedded
        var phones: ProfilePhone?,
        @Embedded
        var links: ProfileLinks?,
        @Embedded
        var company: ProfileCompany?,
        @SerializedName("company_name")
        var companyName: String? = "",
        @PrimaryKey
        var id: Int = 0,
        @SerializedName("first_name")
        var firstName: String? = "",
        var email: String? = "")

@Entity(tableName = "profile_phone")
data class ProfilePhone(var main:String, var hide:Int):Parcelable{
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readInt()) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(main)
                parcel.writeInt(hide)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<ProfilePhone> {
                override fun createFromParcel(parcel: Parcel): ProfilePhone {
                        return ProfilePhone(parcel)
                }

                override fun newArray(size: Int): Array<ProfilePhone?> {
                        return arrayOfNulls(size)
                }
        }

}

@Entity(tableName = "profile_links")
data class ProfileLinks(var image:String, var linkedIn:String,var facebook:String)

@Entity(tableName = "profile_company")
data class ProfileCompany(var company_id:Int, var name:String)