package com.ureclub.model.filter

data class Events(val year: List<Integer>?,
                  val category: List<CategoryItem>?)