package com.ureclub.viewHolders

import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.ureclub.R
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.utils.CircleTransform
import kotlinx.android.synthetic.main.member_icon_item.view.*

class ViewHolderMemberIcon(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: PeopleItem, listener: (PeopleItem) -> Unit) = with(itemView) {
        item.links?.let {
            it.image?.let { image ->
                if (image.isNotEmpty()) {
                    Picasso.with(context).load(it.image)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .error(R.mipmap.user_profile)
                            .placeholder(R.mipmap.user_profile)
                            .transform(CircleTransform()).into(icon)
                }
            }

            if(it.image == null){
                icon.setImageResource(R.mipmap.user_profile)
            }

        }
        setOnClickListener { listener(item) }
    }
}