package com.ureclub.viewHolders

import android.view.View
import android.view.animation.Animation.RELATIVE_TO_SELF
import android.view.animation.RotateAnimation
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import kotlinx.android.synthetic.main.header_list.view.*




class ViewHolderHeaderNews(itemView: View) : GroupViewHolder(itemView) {
    fun bind(group: ExpandableGroup<*>) = with(itemView) {
        headTitle.text = group.title
    }

    override fun expand() {
        animateExpand()

    }

    override fun collapse() {
        animateCollapse()
    }

    private fun animateExpand() =  with(itemView) {
        val rotate = RotateAnimation(360f, 180f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        arrow.setAnimation(rotate)
    }

    private fun animateCollapse() =  with(itemView) {
        val rotate = RotateAnimation(180f, 360f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        arrow.setAnimation(rotate)
    }
}