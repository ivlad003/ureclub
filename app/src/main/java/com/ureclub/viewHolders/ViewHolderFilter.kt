package com.ureclub.viewHolders


import android.view.View
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import com.ureclub.R
import com.ureclub.model.filter.CategoryItem
import kotlinx.android.synthetic.main.item_filter.view.*

class ViewHolderFilter(itemView: View) : ChildViewHolder(itemView) {
    fun bind(item: CategoryItem, listener: (CategoryItem) -> Unit) = with(itemView) {
        title.text = item.name

        itemView.setBackgroundResource(if (item.isSelected) R.color.filter_bg_selected else android.R.color.white)

        setOnClickListener {
            item.isSelected = !item.isSelected
            listener(item)
            itemView.setBackgroundResource(if (item.isSelected) R.color.filter_bg_selected else android.R.color.white)
        }
    }
}

