package com.ureclub.fragments.contact

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ureclub.App
import com.ureclub.R
import kotlinx.android.synthetic.main.contacts_office.*


class ContactOffice : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.contacts_office,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        linkedin.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(App.profile.linkedInLink))
            startActivity(intent)
        }
        facebook.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(App.profile.facebookLink))
            startActivity(intent)
        }
        map.setOnClickListener {
            openMap()
        }
        address.setOnClickListener {
            openMap()
        }
        phone.setOnClickListener {
            tellToPhone(phone.text.toString())
        }
        phone1.setOnClickListener {
            tellToPhone(phone1.text.toString())
        }
        email.setOnClickListener {
            sendEmail()
        }
        webPage.setOnClickListener {
            val url = webPage.text.toString()
            if (url.isNotEmpty()) {
                try {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    startActivity(intent)
                } catch (e: Exception) {

                }
            }
        }
        linkedin.setOnClickListener {
            val inUrl = "https://www.linkedin.com/company/ukrainian-real-estate-club"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(inUrl))
            startActivity(intent)
        }
        facebook.setOnClickListener {
            val fbUrl = "https://www.facebook.com/Ukrainian.Real.Estate.Club"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(fbUrl))
            startActivity(intent)
        }
    }

    private fun sendEmail() {
        val uri = Uri.parse("mailto:" + email.text)
                .buildUpon()
                .build()

        val emailIntent = Intent(Intent.ACTION_SENDTO, uri)
        startActivity(Intent.createChooser(emailIntent, "Email send"))
    }

    private fun openMap() {
        val baseLink = "http://maps.google.com/maps?&z=10"
        val coordinates = "${50.4717454}+${30.5212255}"
        val searchName = "Ukrainian Real Estate Club"
        val linkWithCoordAndName = "${baseLink}&ll=${coordinates}&q=${searchName}"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(linkWithCoordAndName))
        startActivity(intent)
    }

    private fun tellToPhone(number: String?) {
        if (number?.isNotEmpty()!!) {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null))
            startActivity(intent)
        }
    }
}