package com.ureclub.viewHolders


import android.view.View
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import com.ureclub.model.event.DataItemEvent
import kotlinx.android.synthetic.main.item_header_filter.view.*


class ViewHolderHeaderMembersFilter(itemView: View) : ChildViewHolder(itemView) {
    fun bind(item: String) = with(itemView) {
        title.text = item
    }
}

