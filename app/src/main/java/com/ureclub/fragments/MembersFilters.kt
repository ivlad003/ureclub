package com.ureclub.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ureclub.App
import com.ureclub.R
import com.ureclub.adapters.FilterAdapter
import com.ureclub.model.FilterEvent
import com.ureclub.model.filter.CategoryItem
import com.ureclub.model.filter.Filter
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.filter.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import android.support.v7.widget.RecyclerView
import com.google.gson.Gson
import com.ureclub.adapters.FilterMembersAdapter
import com.ureclub.model.FilterMembers


class MembersFilters : Fragment() {
    var sortBy = CategoryItem()
    val categorysSelected = ArrayList<CategoryItem>()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.filter,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton.setOnClickListener {
            setResulte()
            activity?.onBackPressed()
        }
        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)
        toolbar_title.text = getString(R.string.screen_filter_title)
        filterList.layoutManager = LinearLayoutManager(context) as RecyclerView.LayoutManager?
//        progress.visibility = View.VISIBLE

        val token = "${App.profile.token}"
        App.Companion.api.allFilters(token).enqueue(object : Callback<Filter> {
            override fun onFailure(call: Call<Filter>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Filter>?, response: Response<Filter>?) {
                val sectionAdapter = SectionedRecyclerViewAdapter()


                val list = ArrayList<CategoryItem>()

                val categoryItem = CategoryItem()
                categoryItem.name = "Recently joined"
                categoryItem.id = Integer(1)
                list.add(categoryItem)

                val categoryItem1 = CategoryItem()
                categoryItem1.name = "A-Z"
                categoryItem1.id = Integer(2)
                list.add(categoryItem1)

                val categoryItem2 = CategoryItem()
                categoryItem2.name = "Z-A"
                categoryItem2.id = Integer(3)
                list.add(categoryItem2)

                setSingleSection(list, sectionAdapter)



                response?.body()?.data?.contacts?.category?.let {
                    val section = FilterAdapter("Occupation", it) {
                        if (it.isSelected) {
                            categorysSelected.add(it)
                        } else {
                            categorysSelected.remove(it)
                        }
                        setResulte()
                    }
                    sectionAdapter.addSection(section)
                }
                filterList.setHasFixedSize(true)

                filterList.adapter = sectionAdapter
            }

        })
    }

    private fun setSingleSection(list: ArrayList<CategoryItem>, sectionAdapter: SectionedRecyclerViewAdapter) {
        sectionAdapter.removeSection("Sort by")
        val section = FilterMembersAdapter("Sort by", list) {
            sortBy = it

            val listMut = list
            listMut.map { itList->
                itList.isSelected = itList.id == it.id
            }
            setSingleSection(listMut,sectionAdapter)
            setResulte()
        }
        sectionAdapter.addSection(section)
        sectionAdapter.notifyDataSetChanged()
    }

    private fun setResulte() {
        val json = Gson().toJson(FilterMembers(sortBy, categorysSelected))
        val intent = Intent()
        intent.action = "filterMembers"
        intent.putExtra("model", json)
        activity?.sendBroadcast(intent)
    }
}