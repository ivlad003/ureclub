package com.ureclub.viewHolders


import android.view.View
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import com.ureclub.R
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.utils.CircleTransform
import kotlinx.android.synthetic.main.member_item.view.*

class ViewHolderMember(itemView: View) : ChildViewHolder(itemView) {
    fun bind(item: PeopleItem, listener: (PeopleItem) -> Unit) = with(itemView) {
        name_member.text = "${item.firstName} ${item.lastName}"
        position.text = item.position
        name_company.text = item.company?.name
        item.links?.let {
            it.image?.let { image ->
                if (image.isNotEmpty()) {
                    Picasso.with(context).load(it.image)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .error(R.mipmap.user_profile)
                            .placeholder(R.mipmap.user_profile)
                            .transform(CircleTransform()).into(icon)
                }
            }
            if(it.image == null){
                icon.setImageResource(R.mipmap.user_profile)
            }
        }
        setOnClickListener { listener(item) }
    }
}

