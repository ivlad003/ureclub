package com.ureclub.model.news

data class Content(val rendered: String = "",
                   val protected: Boolean = false)