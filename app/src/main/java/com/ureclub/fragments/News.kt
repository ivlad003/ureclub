package com.ureclub.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.activitys.Main
import com.ureclub.adapters.NewsWithoutSectionAdapter
import com.ureclub.model.newsNew.NewsModel


import com.ureclub.utils.SystemUtils
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.news.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class News : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.news,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton.setOnClickListener {
            (activity as Main).drawer.openDrawer(Gravity.START)
        }
        toolbar_title.text = getString(R.string.menu_item_news)
        recyclerView.layoutManager = LinearLayoutManager(context)
        progress.visibility = View.VISIBLE
        loadData()
        tryAgain.setOnClickListener {
            loadData()
        }
    }

    private fun loadData() {
        containerInfo.visibility = View.GONE
        val token = "${App.profile.token}"
        App.Companion.api.listNews(token, SystemUtils.getLang()).enqueue(object : Callback<NewsModel> {
            override fun onResponse(call: Call<NewsModel>, response: Response<NewsModel>) {
                recyclerView.adapter = response.body()?.data?.let {

                    var itlist = it
                    itlist = itlist.sortedWith(compareBy({ it.date })).reversed()

                    itlist.forEach {
                        if (arguments?.getInt("id") == it.post) {
                            val model = Gson().toJson(it)
                            context?.let { it1 -> ActivityFragment.start(it1, NewsInfo::class.java.name, model) }
                        }
                    }

                    NewsWithoutSectionAdapter(itlist) {
                        val model = Gson().toJson(it)
                        context?.let { it1 -> ActivityFragment.start(it1, NewsInfo::class.java.name, model) }
                    }
                }
                containerInfo.visibility = View.GONE
                progress.visibility = View.GONE
            }

            override fun onFailure(call: Call<NewsModel>, t: Throwable) {
                infoImage.setBackgroundResource(R.drawable.ic_server_error)
                containerInfo.visibility = View.VISIBLE
                progress.visibility = View.GONE
            }
        })
    }

}