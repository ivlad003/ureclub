package com.ureclub.model.contacts

import android.os.Parcel
import android.os.Parcelable


data class ContactsModel(val code: String, val data: Contacts)

data class Contacts(val companies: List<CompaniesItem>?,
                    val people: List<PeopleItem>?)


data class CompaniesModel(val company: CompaniesItem,
                     val people: List<PeopleItem>?):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(CompaniesItem::class.java.classLoader),
            parcel.createTypedArrayList(PeopleItem)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(company, flags)
        parcel.writeTypedList(people)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "CompaniesModel(company=$company, people=$people)"
    }

    companion object CREATOR : Parcelable.Creator<CompaniesModel> {
        override fun createFromParcel(parcel: Parcel): CompaniesModel {
            return CompaniesModel(parcel)
        }

        override fun newArray(size: Int): Array<CompaniesModel?> {
            return arrayOfNulls(size)
        }
    }

}