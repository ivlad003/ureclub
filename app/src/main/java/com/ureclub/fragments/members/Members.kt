package com.ureclub.fragments.members

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.activitys.Main
import com.ureclub.extention.inTransaction
import com.ureclub.fragments.EventsFilters
import com.ureclub.fragments.MembersFilters
import kotlinx.android.synthetic.main.events.*
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.toolbar.*

class Members : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.members,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        title.text = getString(R.string.menu_item_members)



        leftButton.setOnClickListener {
            (activity as Main).drawer.openDrawer(Gravity.START)
        }
//        rightButton.visibility = View.GONE
        rightButton.setImageResource(R.mipmap.ic_filter)
        context?.let { rightButton.setColorFilter(ContextCompat.getColor(it, android.R.color.white)) }
        activity?.supportFragmentManager?.inTransaction {
            add(R.id.memberFrame, Peopels())
        }

        rightButton.setOnClickListener {
            context?.let { it1 -> ActivityFragment.start(it1, MembersFilters::class.java.name, "") }
        }

        radioButton.setOnCheckedChangeListener { radioGroup, i ->
            when (i) {
                R.id.radio_people -> {
                    activity?.supportFragmentManager?.inTransaction {
                        val fragment = activity?.supportFragmentManager?.findFragmentById(R.id.memberFrame)
                        fragment?.let {
                            remove(it)
                        }
                        add(R.id.memberFrame, Peopels())
                    }
                }
                else -> {

                    activity?.supportFragmentManager?.inTransaction {
                        val fragment = activity?.supportFragmentManager?.findFragmentById(R.id.memberFrame)
                        fragment?.let {
                            remove(it)
                        }
                        add(R.id.memberFrame, Companies())
                    }
                }
            }
        }
    }
}