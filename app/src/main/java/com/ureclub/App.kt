package com.ureclub

import android.annotation.SuppressLint
import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.onesignal.OneSignal
import com.ureclub.api.rest.ApiManager
import com.ureclub.api.rest.ApiService
import com.ureclub.database.AppDatabase
import com.ureclub.model.Profile


//@AcraMailSender(mailTo = "ivlad003@gmail.com")
class App : Application() {
    companion object {
        lateinit var db: AppDatabase
        lateinit var api: ApiService
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
        lateinit var profile: Profile
        var local: String = "ru"
        @SuppressLint("StaticFieldLeak")
        lateinit var instance: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        db = Room.databaseBuilder(applicationContext,
                AppDatabase::class.java, "ureclub").build()
        api = ApiManager.instance.service
        context = baseContext

//        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.WARN)
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(ExampleNotificationOpenedHandler())
                .unsubscribeWhenNotificationsAreDisabled(false)
                .init()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
//        ACRA.init(this)
    }
}