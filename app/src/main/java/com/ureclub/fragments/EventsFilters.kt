package com.ureclub.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ureclub.App
import com.ureclub.R
import com.ureclub.adapters.FilterAdapter
import com.ureclub.model.FilterEvent
import com.ureclub.model.filter.CategoryItem
import com.ureclub.model.filter.Filter
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.filter.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import com.google.gson.Gson


class EventsFilters : Fragment() {
    val yersSelected = ArrayList<CategoryItem>()
    val categorysSelected = ArrayList<CategoryItem>()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.filter,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton.setOnClickListener {
            setResulte()
            activity?.onBackPressed()
        }
        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)
        toolbar_title.text = getString(R.string.screen_filter_title)
        filterList.layoutManager = LinearLayoutManager(context)
//        progress.visibility = View.VISIBLE

        val token = "${App.profile.token}"
        App.Companion.api.allFilters(token).enqueue(object : Callback<Filter> {
            override fun onFailure(call: Call<Filter>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Filter>?, response: Response<Filter>?) {
                val sectionAdapter = SectionedRecyclerViewAdapter()

                response?.body()?.data?.events?.year?.let {
                    val list = ArrayList<CategoryItem>()
                    it.forEach { year ->
                        val categoryItem = CategoryItem()
                        categoryItem.name = year.toString()
                        list.add(categoryItem)
                    }
                    val section = FilterAdapter(getString(R.string.filter_section_year), list) {
                        if (it.isSelected) {
                            yersSelected.add(it)
                        } else {
                            yersSelected.remove(it)
                        }
                        setResulte()
                    }
                    sectionAdapter.addSection(section)
                }

                response?.body()?.data?.events?.category?.let {
                    val section = FilterAdapter(getString(R.string.filter_section_category), it) {
                        if (it.isSelected) {
                            categorysSelected.add(it)
                        } else {
                            categorysSelected.remove(it)
                        }
                        setResulte()
                    }
                    sectionAdapter.addSection(section)
                }
                filterList.setHasFixedSize(true)

                filterList.adapter = sectionAdapter
            }

        })
    }

    private fun setResulte() {
        val json = Gson().toJson(FilterEvent(yersSelected, categorysSelected))
        val intent = Intent()
        intent.action = "filter"
        intent.putExtra("model", json)
        activity?.sendBroadcast(intent)
    }
}