package com.ureclub.model.event

import android.os.Parcel
import android.os.Parcelable

data class Location(val country: String = "",
                    val address: String = "",
                    val city: String = "",
                    val latitude: Double = 0.0,
                    val name: String = "",
                    val id: Int = 0,
                    val longitude: Double = 0.0):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readDouble(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readDouble()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(country)
        parcel.writeString(address)
        parcel.writeString(city)
        parcel.writeDouble(latitude)
        parcel.writeString(name)
        parcel.writeInt(id)
        parcel.writeDouble(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }
}