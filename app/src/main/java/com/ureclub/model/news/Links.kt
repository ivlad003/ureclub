package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable

data class Links(val self: List<SelfItem>?,
                 val collection: List<CollectionItem>?): Parcelable {
    constructor(parcel: Parcel) : this(
            TODO("self"),
            parcel.createTypedArrayList(CollectionItem)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(collection)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Links> {
        override fun createFromParcel(parcel: Parcel): Links {
            return Links(parcel)
        }

        override fun newArray(size: Int): Array<Links?> {
            return arrayOfNulls(size)
        }
    }
}