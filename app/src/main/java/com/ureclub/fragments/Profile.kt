package com.ureclub.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.activitys.Main
import com.ureclub.utils.CircleTransform
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.profile.*


class Profile : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.profile,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        progress.visibility = View.VISIBLE
        setProfileData()
        notif.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
        menu.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)


        menu.setOnClickListener {
            (activity as Main).drawer.openDrawer(Gravity.START)
        }

        notif.setOnClickListener {
            context?.let { it1 -> ActivityFragment.start(it1, Notifications::class.java.name, "") }
        }

        notif.visibility = View.GONE

        edite.setOnClickListener {
            context?.let { it1 -> ActivityFragment.start(it1, ProfileEdite::class.java.name, "") }
        }

        val ind = App.Companion.profile.linkedInLink

        linkedin.visibility = if(ind.isNullOrEmpty()) View.INVISIBLE else View.VISIBLE

        linkedin.setOnClickListener {

            if (ind?.isNotEmpty()!!) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(ind))
                startActivity(intent)
            }
        }
        val fb = App.Companion.profile.facebookLink

        facebook.visibility = if(fb.isNullOrEmpty()) View.INVISIBLE else View.VISIBLE

        facebook.setOnClickListener {

            if (fb?.isNotEmpty()!!) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(fb))
                startActivity(intent)
            }
        }

        email.setOnClickListener {
            val uri = Uri.parse("mailto:" + App.profile.email)
                    .buildUpon()
                    .build()

            val emailIntent = Intent(Intent.ACTION_SENDTO, uri)
            startActivity(Intent.createChooser(emailIntent, "Email send"))
        }

        phone.setOnClickListener {
            val phone = App.profile.phones?.main
            if (phone?.isNotEmpty()!!) {
                val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
                startActivity(intent)
            }
        }
    }

    private fun setProfileData() {
        AsyncTask.execute {
            val profile = App.db.profileDao().getProfile()
            App.profile = profile
            activity?.runOnUiThread {
                progress.visibility = View.GONE
                Picasso.with(context).load(profile.imageLink)
                        .resize(300, 300)
                        .noFade()
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .placeholder(R.mipmap.user_profile)
                        .error(R.mipmap.user_profile)
                        .transform(CircleTransform()).into(icon)
                name.text = "${profile.firstName} ${profile.lastName}"
                profession.text = profile.jobPosition
                email.text = profile.email

                val phoneStr = profile.phones?.main
                phone.text = ""
                if(profile.phones?.hide == 0)
                    phone.text = phoneStr
                description.text = profile.description
                name_company.text = profile.companyName
            }
        }
    }


    private val mBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            setProfileData()
        }
    }

    override fun onStart() {
        super.onStart()
        activity?.registerReceiver(mBroadCastReceiver, IntentFilter(
                "updateProfile"))
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(mBroadCastReceiver)
    }
}