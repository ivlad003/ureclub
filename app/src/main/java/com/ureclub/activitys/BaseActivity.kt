package com.ureclub.activitys

import android.content.Context
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import com.ureclub.App
import java.util.*


open class BaseActivity : AppCompatActivity() {
    public override fun attachBaseContext(newBase: Context?) {
        newBase?.let {
            val settings = PreferenceManager.getDefaultSharedPreferences(App.context)
            val language = settings?.getString("language", "")
            val local = updateResources(newBase, language.toString())
            super.attachBaseContext(local)
        }

    }


    private fun updateResources(context: Context, language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val configuration = context.getResources().getConfiguration()
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        return context.createConfigurationContext(configuration)
    }

}
