package com.ureclub.adapters
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ureclub.R
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.viewHolders.ViewHolderContacts
import com.ureclub.viewHolders.ViewHolderMemberIcon

class MemberIconAdapter(private val peopleItems: List<PeopleItem>, private val listener: (PeopleItem) -> Unit):
        RecyclerView.Adapter<ViewHolderMemberIcon>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMemberIcon = ViewHolderMemberIcon(
            LayoutInflater.from(parent.context).inflate(R.layout.member_icon_item,
                    parent, false))

    override fun onBindViewHolder(holder: ViewHolderMemberIcon, position: Int) =
            holder.bind(peopleItems[position],listener)

    override fun getItemCount() = peopleItems.size
}