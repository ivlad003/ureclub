package com.ureclub.fragments

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.nj.imagepicker.ImagePicker
import com.nj.imagepicker.listener.ImageResultListener
import com.nj.imagepicker.utils.DialogConfiguration
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.ureclub.App
import com.ureclub.R
import com.ureclub.model.FilterEvent
import com.ureclub.model.ModelImageUpdate
import com.ureclub.model.Profile
import com.ureclub.model.UpdateProfile
import com.ureclub.utils.CircleTransform
import kotlinx.android.synthetic.main.profile_edite.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class ProfileEdite : Fragment() {
    val image: Bitmap? = null
    var file: File? = null
    lateinit var profile: Profile
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.profile_edite,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        progress.visibility = View.VISIBLE
        setProfileInfo()
        menu.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
        menu.setOnClickListener {
            activity?.onBackPressed()
        }
        icon.setOnClickListener {
            ImagePicker.build(DialogConfiguration()
                    .setTitle("Choose")
                    .setResultImageDimension(500,500)
                    .setOptionOrientation(LinearLayoutCompat.HORIZONTAL), ImageResultListener { imageResult ->

                var out: FileOutputStream? = null
                val path = Environment.getExternalStorageDirectory().toString()
                val filePath = "${path}${File.separator}tmp.png"
                try {
                    out = FileOutputStream(filePath)
                    imageResult.bitmap.compress(Bitmap.CompressFormat.PNG, 100, out) // bmp is your Bitmap instance
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (e: Exception) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (e: IOException) {
                        e.printStackTrace();
                    }
                }
                file = File(filePath)
                Picasso.with(context).load(file)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .placeholder(R.mipmap.user_profile)
                        .error(R.mipmap.user_profile).transform(CircleTransform()).into(icon)
            }).show(childFragmentManager)
        }
        save.setOnClickListener {
            updateProfile()
        }

        tryAgain.setOnClickListener {
            updateProfile()
        }
    }

    private fun setProfileInfo() {
        profile = App.profile
        progress.visibility = View.GONE
        profile.imageLink?.let {
            if (it.isNotEmpty()) {
                Picasso.with(context).load(it)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .placeholder(R.mipmap.user_profile)
                        .error(R.mipmap.user_profile).transform(CircleTransform()).into(icon)
            }
        }
        firstname.setText(profile.firstName)
        lastname.setText(profile.lastName)
        profession.setText(profile.jobPosition)
        email.setText(profile.email)
        description.setText(profile.description)

        val phoneStr = profile.phones?.main
        phone.setText("")
        if(profile.phones?.hide==0)
            phone.setText(phoneStr)

        val isChecked = profile.phones?.hide == 1
        hide_phone.isChecked = isChecked
    }

    fun updateProfile() {
        containerInfo.visibility = View.GONE
        progress.visibility = View.VISIBLE

        val token = "${App.profile.token}"
        var phoneStr =  phone.text.toString()

        val hide_phone = if(hide_phone.isChecked) 1 else 0

        App.api.updateProfile(token,
                firstname.text.toString(),
                lastname.text.toString(),
                profession.text.toString(),
                email.text.toString(),
                phoneStr,
                hide_phone,
                facebook.text.toString(),
                linkedin.text.toString(),
                description.text.toString()).enqueue(object : Callback<UpdateProfile> {
            override fun onFailure(call: Call<UpdateProfile>?, t: Throwable?) {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                containerInfo.visibility = View.VISIBLE
                progress.visibility = View.GONE
            }

            override fun onResponse(call: Call<UpdateProfile>?, response: Response<UpdateProfile>?) {
                AsyncTask.execute {
                    response?.body()?.data?.user?.let {
                        profile.firstName = it.firstname
                        profile.lastName = it.lastname
                        profile.description = it.description
                        profile.linkedInLink = it.linkedin
                        profile.email = it.email
                        profile.phones?.hide = it.hide_phone
                        profile.phones?.main = it.phone
                        profile.jobPosition = it.position
                        App.db.profileDao().update(profile)
                        activity?.runOnUiThread {
                            App.profile = profile
                            setProfileInfo()
                            setResulte()
                            updatePhoto()
                        }
                    }
                }
            }
        })
    }

    fun updatePhoto() {
        file?.let {
            val reqFile = RequestBody.create(MediaType.parse("image/*"), it)
            val body = MultipartBody.Part.createFormData("profile_image", it.getName(), reqFile)
            val token = "${App.profile.token}"
            App.api.photoUpdate(token, body).enqueue(object : Callback<ModelImageUpdate> {
                override fun onFailure(call: Call<ModelImageUpdate>?, t: Throwable?) {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                    containerInfo.visibility = View.VISIBLE
                    progress.visibility = View.GONE
                }

                override fun onResponse(call: Call<ModelImageUpdate>?, response: Response<ModelImageUpdate>?) {
                    Toast.makeText(context, "Ok g${response?.body().toString()}", Toast.LENGTH_SHORT).show()
                    profile.imageLink = response?.body()?.data?.imageURL
                    AsyncTask.execute {
                        App.db.profileDao().update(profile)
                        App.profile = profile
                        activity?.runOnUiThread {
                            setProfileInfo()
                            setResulte()
                            containerInfo.visibility = View.GONE
                            progress.visibility = View.GONE
                        }
                    }
                }

            })
        }

        if(file == null){
            containerInfo.visibility = View.GONE
            progress.visibility = View.GONE
        }
    }

    private fun setResulte() {
        val intent = Intent()
        intent.action = "updateProfile"
        activity?.sendBroadcast(intent)
    }
}