package com.ureclub.model.event

data class EventModel(val code: String? = null,
                      val data: List<DataItemEvent>?)