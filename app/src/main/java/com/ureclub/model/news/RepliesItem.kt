package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable

data class RepliesItem(val href: String = "",
                       val embeddable: Boolean = false): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(href)
        parcel.writeByte(if (embeddable) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RepliesItem> {
        override fun createFromParcel(parcel: Parcel): RepliesItem {
            return RepliesItem(parcel)
        }

        override fun newArray(size: Int): Array<RepliesItem?> {
            return arrayOfNulls(size)
        }
    }
}