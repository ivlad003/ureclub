package com.ureclub.model.news.WpTerm

import android.os.Parcel
import android.os.Parcelable

data class CuriesItem(val templated: Boolean = false,
                      val name: String = "",
                      val href: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (templated) 1 else 0)
        parcel.writeString(name)
        parcel.writeString(href)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CuriesItem> {
        override fun createFromParcel(parcel: Parcel): CuriesItem {
            return CuriesItem(parcel)
        }

        override fun newArray(size: Int): Array<CuriesItem?> {
            return arrayOfNulls(size)
        }
    }
}