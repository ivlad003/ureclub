package com.ureclub.model

import java.util.*

data class AnttendanceModel(val code: Object? = null,
                            val data: List<DataItem>?)


data class DataItem(val booking: Int = 0,
                    val person: Int = 0)


