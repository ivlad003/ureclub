package com.ureclub.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ureclub.R
import com.ureclub.activitys.Main
import kotlinx.android.synthetic.main.code_of_ethics.*
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*

class CodeOfEthics : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.code_of_ethics,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        toolbar_title.text = getString(R.string.menu_item_codex)

        val text = """Мы исповедуем принципы честной конкуренции и этичности ведения бизнеса
        Развитие нашего бизнеса напрямую связано с развитием города, страны – совершенствуя компанию, мы повышаем стандарты качества рынка недвижимости и строительства
        Мы являемся публичными компаниями с прозрачными и открытыми структурами
        Мы придерживаемся антикоррупционной стратегии и работаем во благо общества, города, страны
        Мы считаем нашим главным активом – человеческий капитал, своим приоритетом видим соблюдение прав и свобод каждого, реализацию потенциала сотрудников, уважение к нашим партнерам и клиентам
        В своих действиях руководствуемся принципами экологичности и бережного отношения к природным ресурсам"""
        content.text = text
        leftButton.setOnClickListener {
            (activity as Main).drawer.openDrawer(Gravity.START)
        }
    }
}