package com.ureclub.viewHolders

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import com.ureclub.R
import com.ureclub.R.id.listPeoples
import com.ureclub.adapters.MemberIconAdapter
import com.ureclub.adapters.MembersAdapter
import com.ureclub.model.contacts.CompaniesItem
import com.ureclub.model.contacts.CompaniesModel
import kotlinx.android.synthetic.main.companie_item_header.view.*


class ViewHolderHeaderCompany(itemView: View) : GroupViewHolder(itemView) {
    fun bind(group: ExpandableGroup<*>) = with(itemView) {
        val item = Gson().fromJson(group.title, CompaniesModel::class.java)
        title.text = item.company.name

        Log.d("ViewHolderHeaderCompany", "ViewHolderHeaderCompany ${item}")

        item.company.links?.let {
            it.image?.let { image ->
                if (image.isNotEmpty())
                    Picasso.with(context).load(it.image)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .placeholder(R.mipmap.company_default)
                            .error(R.mipmap.company_default).into(icon)
            }

            Log.d("ViewHolderHeaderCompany", "ViewHolderHeaderCompany image ${it.image}")



            if (it.image == null) {
                icon.setImageResource(R.mipmap.company_default)
            }

            if (it.image?.toString()?.isEmpty()!!) {
                icon.setImageResource(R.mipmap.company_default)
            }
        }

        if (item.company.links == null) {
            icon.setImageResource(R.mipmap.company_default)
        }

        val layoutManager = LinearLayoutManager(listPeoples.context,
                LinearLayoutManager.HORIZONTAL, false)
        listPeoples.layoutManager = layoutManager
        listPeoples.adapter = item.people?.let {
            MemberIconAdapter(it) {

            }
        }
    }

    override fun expand() {
        super.expand()
        itemView.findViewById<RecyclerView>(R.id.listPeoples).visibility = View.GONE
    }

    override fun collapse() {
        super.collapse()
        itemView.findViewById<RecyclerView>(R.id.listPeoples).visibility = View.VISIBLE
    }


}