package com.ureclub.api.rest

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.logging.Level


/**
 * Created by vladzatcer on 12/29/17.
 */
class ApiManager  private constructor(){
    var service: ApiService

    private object Holder { val INSTANCE = ApiManager() }

    companion object {
        val instance: ApiManager by lazy { Holder.INSTANCE }
    }

    init {
        val logging = HttpLoggingInterceptor()
// set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)  //

        val retrofit = Retrofit.Builder()
                .baseUrl("http://urec.1gb.ua")
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(httpClient.build())
                .build();
        service = retrofit.create(ApiService::class.java)
    }
}