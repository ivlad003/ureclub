package com.ureclub.model.filter

data class Filter(val code: String? = null,
                  val data: Data)