package com.ureclub.activitys

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.ureclub.R
import com.ureclub.extention.inTransaction
import com.ureclub.fragments.PeopleProfile
import com.ureclub.fragments.Profile
import com.ureclub.model.contacts.PeopleItem
import android.content.Intent
import com.ureclub.App
import kotlinx.android.synthetic.main.profile.*
import java.io.Serializable


/**
 * Created by zvlad on 2/6/18.
 */

class ActivityFragment : BaseActivity() {

    companion object {
        fun start(context: Context, nameFragment: String, model: String) {
            val retIntent = Intent(App.context, ActivityFragment::class.java)
            retIntent.putExtra("name", nameFragment)
            retIntent.putExtra("model", model)
            context.startActivity(retIntent)
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)
        supportActionBar?.hide()

        val intent = intent
        val nameFragment: String? = intent.extras.getString("name")
        val model = intent.extras.getString("model")
        val bundle = Bundle()
        bundle.putSerializable("model", model)
        nameFragment?.let {
            supportFragmentManager.inTransaction {
                add(R.id.frame, Fragment.instantiate(this@ActivityFragment, it, bundle))
            }
        }
    }

}
