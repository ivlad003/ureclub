package com.ureclub.fragments.events


import android.graphics.Point
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.google.gson.Gson
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.fragments.SeeWhoAttend
import com.ureclub.model.AnttendanceModel
import com.ureclub.model.event.DataItemEvent
import com.ureclub.utils.DialogInterface
import com.ureclub.utils.SystemUtils
import kotlinx.android.synthetic.main.event_about.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern




class EventAbout : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.event_about,
            container, false)

    var isGo = false
    var anttendanceModel:AnttendanceModel? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val model = Gson().fromJson(arguments?.getString("model"), DataItemEvent::class.java)

        val p = Pattern.compile("src=\"(.*?)\"")
        val m = p.matcher(model.content)

        val urls = ArrayList<String>()
        var i = 0
        while (m.find()) {
            for (j in 0..m.groupCount()) {
                val url = m.group(j)
                if (url.indexOf("src=\"") < 0)
                    urls.add(url)
                i++
            }
        }
        urls.forEach {
            val textSliderView = TextSliderView(context)
            textSliderView
                    .image(it)
                    .setScaleType(BaseSliderView.ScaleType.Fit)

            textSliderView.empty(R.mipmap.image_empty)
            textSliderView.error(R.mipmap.image_empty)
            slider.addSlider(textSliderView)
        }


        if (urls.size == 1) {
            slider.stopAutoCycle()
        }
//        web.loadDataWithBaseURL("", model.content, "text/html",
//                "UTF-8", "")


        val display = activity?.getWindowManager()?.getDefaultDisplay()
        val size = Point()
        display?.getSize(size)
        val width = size.x

//        val css = "body {\n" +
//                "    background-color: white;\n" +
//                "    font-family: Montserrat-Regular;\n" +
//                "    font-size: 128px;\n" +
//                "    color: #333;\n" +
//                "    font-size: 1rem;\n" +
//                "    font-weight: 400;\n" +
//                "    line-height: 1.66;\n" +
//                "    word-wrap: break-word;\n" +
//                "    padding: 0px;\n" +
//                "}\n" +
//                "\n" +
//                "strong, b {\n" +
//                "    font-weight: bold;\n" +
//                "}\n" +
//                "\n" +
//                "p {\n" +
//                "    margin-bottom: 24px;\n" +
//                "    line-height: 26px;\n" +
//                "    display: block;\n" +
//                "    padding: 0;\n" +
//                "}\n" +
//                "\n" +
//                "img {\n" +
//                "    height: auto;\n" +
//                "    width: 100%;\n" +
//                "    max-width: 100%;\n" +
//                "}"

    val css = "body {\n" +
            "@font-face {"+
                    " font-family: \"MyFont\";"+
                    "  src: url(\"file:///android_asset/fonts/montserrat_regular.ttf\");"+
                    "}"+
                    "    background-color: white;\n" +
                    "    font-family: MyFont\n" +
                    "    font-size: 38px;\n" +
                    "    color: #333;\n" +
                    "    font-size: 3rem;\n" +
                    "    font-weight: 300;\n" +
                    "    line-height: 1.1;\n" +
                    "    word-wrap: break-word;\n" +
                    "    padding: 0px;\n" +
                    "}\n" +
                    "\n" +
                    "strong, b {\n" +
                    "    font-weight: bold;\n" +
                    "}\n" +
                    "\n" +
                    "p {\n" +
                    "    margin-bottom: 24px;\n" +
                    "    line-height: 46px;\n" +
                    "    display: block;\n" +
                    "    padding: 0;\n" +
                    "}\n" +
                    "\n" +
                    "img {\n" +
                    "    height: auto;\n" +
                    "    width: ${width};\n" +
                    "    max-width: ${width};\n" +
                    "}"

        val htmlData = "<html>\n" +
                "<head><style>${css}</style></head>\n" +
                "<body>" + model.content+"</body>\n" +
                "</html>"
        web.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "UTF-8", null);
        web.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN)
        web.getSettings().setLoadWithOverviewMode(true)
        web.getSettings().setUseWideViewPort(true)
        title.text = model.title
        date.text = model.date?.dateBeg
        workTime.text = "${model.date?.timeBeg} - ${model.date?.timeEnd}"
        location.text = model.location?.name


        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)
        toolbar_title.text = ""

        leftButton.setOnClickListener {
            activity?.onBackPressed()
        }
        getAttendance(model)
        btn_go.setOnClickListener {
            val date = SimpleDateFormat("yyyy-MM-dd").parse(model.date?.dateEnd)
            if (isGo && date.after(Date())) {
                context?.let { it1 ->
                    SystemUtils.showDialogBookingDecline(it1,object :DialogInterface{
                        override fun positive() {
                            goCancel(model)
                        }
                    })
                }
            } else {
                context?.let { it1 ->
                    SystemUtils.showDialogBookingSuccess(it1,object :DialogInterface{
                        override fun positive() {
                            goCall(model)
                        }
                    })
                }
            }
        }

        see_who_attend.setOnClickListener{
            val modelJson = Gson().toJson(anttendanceModel)
            context?.let { it1 -> ActivityFragment.start(it1, SeeWhoAttend::class.java.name, modelJson) }
        }

    }

    var bookingId = 0
    private fun getAttendance(model: DataItemEvent) {
        progress.visibility = View.VISIBLE
        val token = "${App.profile.token}"
        App.api.attendance(token, model.id).enqueue(object : Callback<AnttendanceModel> {
            override fun onFailure(call: Call<AnttendanceModel>?, t: Throwable?) {
                progress.visibility = View.GONE
            }

            override fun onResponse(call: Call<AnttendanceModel>?, response: Response<AnttendanceModel>?) {
                response?.body()?.let {
                    anttendanceModel = it
                    val item = it.data?.find { dataItem ->
                        dataItem.person == App.profile.id

                    }
                    if (item == null) {
                        setGo()
                    } else {
                        bookingId = item.booking
                        setDeclilne()
                    }
                    progress.visibility = View.GONE
                }
            }

        })


    }

    fun goCall(model: DataItemEvent) {
        progress.visibility = View.VISIBLE
        val token = "${App.profile.token}"
        App.api.nonce(token, token, NonceAction.bookingAdd.bookingType).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                progress.visibility = View.GONE
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                val jsonString = response?.body()?.bytes()?.let { String(it) }
                val jsonObject = JSONObject(jsonString)
                val data = HashMap<String, String>()
                data.put("action", NonceAction.bookingAdd.bookingType)
                val _wpnonce = jsonObject.getJSONObject("data").getString("nonce")
                data.put("_wpnonce", _wpnonce)
                data.put("event_id", model.id.toString())
                data.put("em_tickets[${model.ticket?.id}][spaces]", 1.toString())
                data.put("booking_comment", "")
                data.put("em_lang", "en_US")
                data.put("lang", "en")
                App.api.book_event(token, data).enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {

                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        getAttendance(model)
                    }

                })
            }
        })
    }

    fun goCancel(model: DataItemEvent) {
        progress.visibility = View.VISIBLE
        val token = "${App.profile.token}"
        App.api.nonce(token, token, NonceAction.bookingAdd.bookingType).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                progress.visibility = View.GONE
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                val jsonString = response?.body()?.bytes()?.let { String(it) }
                val jsonObject = JSONObject(jsonString)
                val data = HashMap<String, String>()
                data.put("action", NonceAction.bookingCancel.bookingType)
                val _wpnonce = jsonObject.getJSONObject("data").getString("nonce")
                data.put("_wpnonce", _wpnonce)
                data.put("booking_id", bookingId.toString())
                App.api.book_event(token, data).enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {

                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        getAttendance(model)
                    }

                })
            }
        })
    }

    private fun setDeclilne() {
        isGo = true
        btn_go.text = "Decline"
        btn_go.background = context?.let { it1 -> ContextCompat.getDrawable(it1, R.drawable.button_corner_decline) }
    }

    private fun setGo() {
        isGo = false
        btn_go.text = "Go!"
        btn_go.background = context?.let { it1 -> ContextCompat.getDrawable(it1, R.drawable.button_corner_go) }
    }
}

enum class NonceAction(val bookingType: String) {
    bookingAdd("booking_add"),
    bookingCancel("booking_cancel")
}