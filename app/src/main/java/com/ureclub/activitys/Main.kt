package com.ureclub.activitys

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.res.Resources
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.squareup.picasso.Picasso
import com.ureclub.App
import com.ureclub.R
import com.ureclub.extention.inTransaction
import com.ureclub.fragments.*
import com.ureclub.fragments.events.Events
import com.ureclub.fragments.members.Members
import com.ureclub.fragments.members.Peopels
import com.ureclub.utils.CircleTransform
import com.ureclub.utils.LocaleManager
import kotlinx.android.synthetic.main.main.*
import java.util.*
import android.preference.PreferenceManager
import android.content.SharedPreferences
import android.support.v4.widget.DrawerLayout
import android.text.Layout
import android.text.style.AlignmentSpan
import android.text.SpannableString
import com.onesignal.OneSignal
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.ureclub.fragments.contact.Contacts


class Main : BaseActivity() {
    var settings: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        settings = PreferenceManager.getDefaultSharedPreferences(this)
        setContentView(R.layout.main)
        setSupportActionBar(toolbar)
        supportActionBar?.hide()


        val postType: String? = intent.extras?.getString("postType")
        val postID: Int? = intent.extras?.getInt("postID")


        postType?.let {
            if (it.equals("news")) {
                val arg = Bundle()
                postID?.let { it1 -> arg.putInt("id", it1) }
                val fragmentNews = News()
                fragmentNews.arguments = arg
                supportFragmentManager.inTransaction {
                    add(R.id.frame, fragmentNews)
                }
            } else if (it.equals("event")) {
                val fragmentEvent = Events()
                val arg = Bundle()
                postID?.let { it1 -> arg.putInt("id", it1) }
                fragmentEvent.arguments = arg
                supportFragmentManager.inTransaction {
                    add(R.id.frame, fragmentEvent)
                }
            }
        }

        if (postType == null)
            supportFragmentManager.inTransaction {
                add(R.id.frame, Events())
            }

        val menu = navigation.menu

        for (i in 0..menu.size() - 1) {
            val item = menu.getItem(i)
            val s = SpannableString(item.title)
            s.setSpan(AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, s.length, 0)
            item.setTitle(s)
        }

        navigation.setNavigationItemSelectedListener {
            drawer.closeDrawers()

            supportFragmentManager?.inTransaction {
                val fragment = supportFragmentManager?.findFragmentById(R.id.frame)
                fragment?.let {
                    remove(it)
                }
            }

            when (it.itemId) {
                R.id.nav_my_profile -> {
                    supportFragmentManager.inTransaction {
                        add(R.id.frame, Profile())
                    }
                    true
                }
                R.id.nav_news -> {

                    supportFragmentManager.inTransaction {
                        add(R.id.frame, News())
                    }
                    true
                }
                R.id.nav_contacts -> {
                    supportFragmentManager.inTransaction {
                        add(R.id.frame, Contacts())
                    }
                    true
                }
                R.id.nav_events -> {
                    supportFragmentManager.inTransaction {
                        add(R.id.frame, Events())
                    }
                    true
                }
                R.id.nav_code_of_ethics -> {
                    supportFragmentManager.inTransaction {
                        add(R.id.frame, CodeOfEthics())
                    }
                    true
                }
                R.id.nav_members -> {
                    supportFragmentManager.inTransaction {
                        add(R.id.frame, Members())
                    }
                    true
                }
                R.id.nav_settings -> {
                    supportFragmentManager.inTransaction {
                        add(R.id.frame, Settings())
                    }
                    true
                }
                else -> {
                    true
                }
            }
        }

        drawer.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {

            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

            }

            override fun onDrawerClosed(drawerView: View) {

            }

            override fun onDrawerOpened(drawerView: View) {
                updateInfo()
            }

        })

        val local = Resources.getSystem().getConfiguration().locale;
        Log.d("MainA", "test " + local)

        logout.setOnClickListener({
            AsyncTask.execute {
                App.db.profileDao().delete(App.Companion.profile)
                runOnUiThread {
                    finish()
                    startActivity(Intent(App.context, Login::class.java))
                }
            }
        })
        updateInfo()
    }

    private fun updateInfo() {
        AsyncTask.execute {
            val profile = App.db.profileDao().getProfile()
            App.profile = profile
            runOnUiThread {
                OneSignal.sendTag("first_name", profile.firstName)
                OneSignal.sendTag("last_name", profile.lastName)
                OneSignal.setEmail(profile.email!!, profile.token?.replace("Bearer ", ""))

                if (!profile.imageLink.isNullOrEmpty()) {
                    Picasso.with(this).load(profile.imageLink)
                            .resize(100, 100)
                            .noFade()
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .placeholder(R.mipmap.user_profile)
                            .error(R.mipmap.user_profile)
                            .transform(CircleTransform()).into(icon_logout)
                }
                name_logout.text = "${profile.firstName} ${profile.lastName}"
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        val fragment = supportFragmentManager.findFragmentById(R.id.frame)
        fragment.onActivityResult(requestCode, resultCode, data)
    }
}


