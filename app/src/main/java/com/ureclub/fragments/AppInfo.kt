package com.ureclub.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ureclub.R
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import android.content.pm.PackageManager
import android.R.attr.versionName
import android.content.pm.PackageInfo
import android.text.method.LinkMovementMethod
import kotlinx.android.synthetic.main.app_info.*
import android.content.Intent
import android.net.Uri


class AppInfo : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.app_info,
                                    container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton.setOnClickListener {
            activity?.onBackPressed()
        }
        toolbar_title.text = getString(R.string.settings_section_about)
        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)

        try {
            val pInfo = activity?.getPackageManager()?.getPackageInfo(activity?.getPackageName(), 0)
            val vers = pInfo?.versionName
            version.text = vers
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        website.setOnClickListener {
            val url = "http://soft4status.com"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        Vlad_Kosmach.setOnClickListener {
            val url = "https://www.linkedin.com/in/ivlad003/"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
    }



}