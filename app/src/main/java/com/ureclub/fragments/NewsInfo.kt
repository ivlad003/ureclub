package com.ureclub.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import com.daimajia.slider.library.SliderLayout
import com.ureclub.R
import java.util.regex.Pattern
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.google.gson.Gson
import com.ureclub.model.news.ModelNews
import com.ureclub.model.newsNew.NewsItem
import com.ureclub.model.newsNew.NewsModel
import kotlinx.android.synthetic.main.news_info.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*


class NewsInfo : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.news_info,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val model = Gson().fromJson(arguments?.getString("model"), NewsItem::class.java)
        val p = Pattern.compile("src=\"(.*?)\"")
        val m = p.matcher(model?.content)

        val urls = ArrayList<String>()
        var i = 0
        while (m.find()) {
            for (j in 0..m.groupCount()) {
                val url = m.group(j)
                if (url.indexOf("src=\"") < 0)
                    urls.add(url)
                i++
            }
        }
        urls.forEach {
            val textSliderView = TextSliderView(context)
            textSliderView
                    .image(it)
                    .setScaleType(BaseSliderView.ScaleType.Fit)

            textSliderView.empty(R.mipmap.image_empty)
            textSliderView.error(R.mipmap.image_empty)
            slider.addSlider(textSliderView)
        }

        slider.setPresetTransformer(SliderLayout.Transformer.Stack)
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        slider.setDuration(3000)

        if(urls.size == 1){
            slider.stopAutoCycle()
        }

        val display = activity?.getWindowManager()?.getDefaultDisplay()
        val width = display?.getWidth()
        val css = "body {\n" +
                "@font-face {"+
                " font-family: \"MyFont\";"+
                "  src: url(\"file:///android_asset/fonts/montserrat_regular.ttf\");"+
                "}"+
                "    background-color: white;\n" +
                "    font-family: MyFont\n" +
                "    font-size: 38px;\n" +
                "    color: #333;\n" +
                "    font-size: 3rem;\n" +
                "    font-weight: 300;\n" +
                "    line-height: 1.1;\n" +
                "    word-wrap: break-word;\n" +
                "    padding: 0px;\n" +
                "}\n" +
                "\n" +
                "strong, b {\n" +
                "    font-weight: bold;\n" +
                "}\n" +
                "\n" +
                "p {\n" +
                "    margin-bottom: 24px;\n" +
                "    line-height: 46px;\n" +
                "    display: block;\n" +
                "    padding: 0;\n" +
                "}\n" +
                "\n" +
                "img {\n" +
                "    height: auto;\n" +
                "    width: ${width};\n" +
                "    max-width: ${width};\n" +
                "}"

        val htmlData = "<html>\n" +
                "<head><style>${css}</style></head>\n" +
                "<body>" + model.content+"</body>\n" +
                "</html>"

        web.getSettings().setJavaScriptEnabled(true);
        web.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html",
                "UTF-8", "");
        web.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN)
        web.getSettings().setLoadWithOverviewMode(true)
        web.getSettings().setUseWideViewPort(true)
        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)
        toolbar_title.text = model.title

        leftButton.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun onStop() {
        slider.stopAutoCycle()
        super.onStop()
    }

    companion object {
        fun newInstance(model: ModelNews, fragment: Fragment): NewsInfo {
            val fragment = NewsInfo()
            val args = Bundle()
            args.putParcelable("model", model)
            fragment.arguments = args
            fragment.setTargetFragment(fragment, 1)
            return fragment
        }
    }
}