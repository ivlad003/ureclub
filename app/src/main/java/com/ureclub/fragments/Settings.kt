package com.ureclub.fragments


import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.activitys.Main
import com.ureclub.adapters.FilterAdapter
import com.ureclub.model.FilterEvent
import com.ureclub.model.filter.CategoryItem
import com.ureclub.model.filter.Filter
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.filter.*
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.settings.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.util.DisplayMetrics
import android.widget.AdapterView
import java.util.*
import android.os.Build
import android.annotation.TargetApi
import android.content.Context
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.preference.PreferenceManager


class Settings : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.settings,
            container, false)

    var isFirstSelect = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton.setOnClickListener {
            (activity as Main).drawer.openDrawer(Gravity.START)
        }
        toolbar_title.text = getString(R.string.menu_item_settings)


        change_password.setOnClickListener {
            context?.let { it1 -> ActivityFragment.start(it1, PasswordEdite::class.java.name, "") }
        }
        app_info.setOnClickListener {
            context?.let { it1 -> ActivityFragment.start(it1, AppInfo::class.java.name, "") }
        }

        val settings = PreferenceManager.getDefaultSharedPreferences(App.context)
        val languageIndex = settings.getInt("languageIndex", 0)

        languages.setSelection(languageIndex)

        languages.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            @SuppressLint("CommitPrefEdits", "ApplySharedPref")
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (isFirstSelect) {
                    val local = when (p2) {
                        1 -> {
                            "uk"
                        }
                        2 -> {
                            "ru"
                        }
                        3 -> {
                            "en"
                        }
                        else -> {
                            ""
                        }
                    }

                    val editor = settings.edit()
                    editor.putString("language", local)
                    editor.putInt("languageIndex", p2)
                    editor.commit()

                    activity?.finish()
                    activity?.startActivity(activity?.getIntent());
                }
                isFirstSelect = true
            }
        }
    }

    private fun updateResources(context: Context, language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val configuration = context.getResources().getConfiguration()
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        return context.createConfigurationContext(configuration)
    }
}