package com.ureclub.model

import com.ureclub.model.filter.CategoryItem
import java.io.Serializable

/**
 * Created by zvlad on 2/24/18.
 */
data class FilterEvent(var yersSelected : ArrayList<CategoryItem>,
                       var categorysSelected :ArrayList<CategoryItem>):Serializable