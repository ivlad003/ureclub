package com.ureclub.activitys

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.ureclub.App
import com.ureclub.R
import com.ureclub.model.ForgotPassword
import com.ureclub.utils.MyBounceInterpolator
import kotlinx.android.synthetic.main.forgot_password.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPassword : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgot_password)


        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)
        toolbar_title.text = "Forgot password"
    }

    fun forgotPassword(v: View) {
        val email = etEmail.text.toString()
        App.Companion.api.forgotPassword(email).enqueue(object : Callback<ForgotPassword> {
            override fun onFailure(call: Call<ForgotPassword>?, t: Throwable?) {

            }

            @SuppressLint("ShowToast")
            override fun onResponse(call: Call<ForgotPassword>?, response: Response<ForgotPassword>?) {
                val id = response?.body()?.data?.user?.id
                Toast.makeText(this@ForgotPassword, response?.body()?.code,
                        Toast.LENGTH_SHORT).show()
                if (id == null) {
                    etEmail.text.clear()
                } else {
                    onBackPressed()
                }
            }

        })

        animationButton(v)
    }

    private fun animationButton(view: View) {
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        val interpolator = MyBounceInterpolator(0.1, 40.0)
        myAnim.interpolator = interpolator
        view.startAnimation(myAnim)
    }

    fun left() {
        onBackPressed()
    }
}