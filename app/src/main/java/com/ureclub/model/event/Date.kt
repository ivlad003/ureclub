package com.ureclub.model.event

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Date(@SerializedName("time_beg")
                val timeBeg: String = "",
                @SerializedName("date_beg")
                val dateBeg: String = "",
                @SerializedName("date_end")
                val dateEnd: String = "",
                @SerializedName("time_end")
                val timeEnd: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(timeBeg)
        parcel.writeString(dateBeg)
        parcel.writeString(dateEnd)
        parcel.writeString(timeEnd)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Date> {
        override fun createFromParcel(parcel: Parcel): Date {
            return Date(parcel)
        }

        override fun newArray(size: Int): Array<Date?> {
            return arrayOfNulls(size)
        }
    }
}