package com.ureclub.adapters
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ureclub.R
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.viewHolders.ViewHolderContacts

class ContactsAdapter(private val peopleItems: List<PeopleItem>, private val listener: (PeopleItem) -> Unit):
        RecyclerView.Adapter<ViewHolderContacts>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolderContacts(
            LayoutInflater.from(parent.context).inflate(R.layout.contacts_item,
                    parent, false))

    override fun onBindViewHolder(holder: ViewHolderContacts, position: Int) =
            holder.bind(peopleItems[position],listener)

    override fun getItemCount() = peopleItems.size
}