package com.ureclub.viewHolders


import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.Picasso
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import com.ureclub.R
import com.ureclub.model.news.ModelNews
import kotlinx.android.synthetic.main.news_item.view.*
import java.text.DateFormatSymbols
import java.util.regex.Pattern

class ViewHolderNews(itemView: View) : ChildViewHolder(itemView) {
    fun bind(item: ModelNews, listener: (ModelNews) -> Unit) = with(itemView) {
        title.text = item.title?.rendered


        val im = image
        val p = Pattern.compile("src=\"(.*?)\"")
        val m = p.matcher(item.content?.rendered)

        if (m.find()) {
            val imageUrl = m.group(1)
            Picasso.with(context).load(imageUrl)
                    .placeholder(R.mipmap.image_empty)
                    .error(R.mipmap.image_empty).into(image)
        }

        val dfs = DateFormatSymbols()
        val months = dfs.shortMonths
        val array = item.date.split("T")[0].split("-")
        val index = array
                .get(1).replace("0","")?.toInt()
        val day = array.get(2)

        index.let {
            monthTitle.text = "${day}\n${months[it-1]}"
        }

        subTitle.setOnClickListener {
            listener(item)
        }

        setOnClickListener { listener(item) }
    }
}

