package com.ureclub.viewHolders

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.squareup.picasso.Picasso
import com.ureclub.R
import com.ureclub.model.event.DataItemEvent
import kotlinx.android.synthetic.main.item_event_list.view.*
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.regex.Pattern

class ViewHolderEventsListItem(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: DataItemEvent, listener: (DataItemEvent) -> Unit) = with(itemView) {
        title.text = item.title
        date.text = item.date?.dateBeg
        workTime.text = "${item.date?.timeBeg?.substring(0,5)} - ${item.date?.timeEnd?.substring(0,5)}"
        location.text ="${item.location?.name},${item.location?.city}"

        Log.d("EventsListItem", item.toString())
        val p = Pattern.compile("src=\"(.*?)\"")
        val m = p.matcher(item.content)
        if (m.find()) {
            val imageUrl = m.group(1)
            Picasso.with(context).load(imageUrl)
                    .placeholder(R.mipmap.image_empty)
                    .error(R.mipmap.image_empty).into(icon)
        } else {
            Picasso.with(context).load(R.mipmap.image_empty)
                    .placeholder(R.mipmap.image_empty)
                    .error(R.mipmap.image_empty).into(icon)
        }



        val dfs = DateFormatSymbols()
        val months = dfs.shortMonths
        val array = item.date?.dateBeg?.split("-")
        val index = array
                ?.get(1)?.replace("0","")?.toInt()


        val day = array?.get(2)

        index?.let {
            monthTitle.text = "${day}\n${months[it-1]}"
        }

        setOnClickListener { listener(item) }
    }
}