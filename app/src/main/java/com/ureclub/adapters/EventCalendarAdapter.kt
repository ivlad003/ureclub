package com.ureclub.adapters

import android.support.v7.widget.RecyclerView
import android.view.View

import com.ureclub.R
import com.ureclub.model.event.DataItemEvent
import com.ureclub.viewHolders.ViewHolderEvent
import com.ureclub.viewHolders.ViewHolderHeaderCalendarEvent

import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

/**
 * Created by zvlad on 2/22/18.
 */

class EventCalendarAdapter(private val title: String, private val list: List<DataItemEvent>, private  val listener: (DataItemEvent) -> Unit) :
        StatelessSection(R.layout.header_item_events_list, R.layout.item_calendar_list) {
    override fun getContentItemsTotal(): Int {
        return list.size
    }

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        return ViewHolderEvent(view)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val iHolder = holder as ViewHolderEvent
        iHolder.bind(list[position],listener)
    }

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return ViewHolderHeaderCalendarEvent(view)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?) {
        val hHolder = holder as ViewHolderHeaderCalendarEvent?
        hHolder?.bind(title)
    }

    companion object {
        private val TAG = EventCalendarAdapter::class.java.simpleName
    }
}
