package com.ureclub.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.ureclub.R
import com.ureclub.model.NewsModel
import com.ureclub.model.news.ModelNews
import com.ureclub.viewHolders.ViewHolderHeaderNews
import com.ureclub.viewHolders.ViewHolderNews

class NewsAdapter(groups: List<ExpandableGroup<*>>,private val listener: (ModelNews) -> Unit)
    : ExpandableRecyclerViewAdapter<ViewHolderHeaderNews, ViewHolderNews>(groups) {
    override fun onCreateGroupViewHolder(parent: ViewGroup, viewType: Int): ViewHolderHeaderNews {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.header_list, parent, false)
        return ViewHolderHeaderNews(view)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup, viewType: Int): ViewHolderNews {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        return ViewHolderNews(view)
    }

    override fun onBindChildViewHolder(holder: ViewHolderNews, flatPosition: Int, group: ExpandableGroup<*>,
                                       childIndex: Int) {
        val item =  group.items.get(childIndex) as ModelNews
//        holder.bind(item,listener)
    }

    override fun onBindGroupViewHolder(holder: ViewHolderHeaderNews, flatPosition: Int,
                                       group: ExpandableGroup<*>) {
        holder.bind(group)
    }
}