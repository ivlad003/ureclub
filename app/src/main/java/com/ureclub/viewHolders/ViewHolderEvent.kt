package com.ureclub.viewHolders


import android.view.View
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder
import com.ureclub.model.event.DataItemEvent
import kotlinx.android.synthetic.main.item_calendar_list.view.*

class ViewHolderEvent(itemView: View) : ChildViewHolder(itemView) {
    fun bind(item: DataItemEvent, listener: (DataItemEvent) -> Unit) = with(itemView) {
        title.text = item.title
        data.text = item.date?.dateBeg?.split("-")?.get(2)
        setOnClickListener { listener(item) }
    }
}

