package com.ureclub.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ureclub.App
import com.ureclub.R
import kotlinx.android.synthetic.main.password_edite.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PasswordEdite : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.password_edite,
            container, false)

    @SuppressLint("ShowToast")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton.setOnClickListener {
            activity?.onBackPressed()
        }
        toolbar_title.text = getString(R.string.settings_cell_password_change)
        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)

        btn_change.setOnClickListener {
            val password1 = newPassword.text.toString()
            val password2 = newPasswordRep.text.toString()
            if (password1.isEmpty() || password2.isEmpty()) {
                Toast.makeText(activity, "Don't empty field!", Toast.LENGTH_SHORT).show()
            } else if (password1.length < 6 || password2.length < 6) {
                Toast.makeText(activity, "Less than 6 characters!", Toast.LENGTH_SHORT).show()
            } else if (password1.equals(password2)) {
                changePassword(password1)
            } else {
                Toast.makeText(activity, "Don't equals fields!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    @SuppressLint("ShowToast")
    private fun changePassword(password1: String) {
        val token = "${App.profile.token}"
        App.api.changePassword(token, password1).enqueue(object : Callback<Void> {

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                Toast.makeText(activity, t?.message, Toast.LENGTH_SHORT)
            }

            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                newPassword.text.clear()
                newPasswordRep.text.clear()
                Toast.makeText(activity, "Change successful!", Toast.LENGTH_SHORT).show()
            }
        })
    }
}