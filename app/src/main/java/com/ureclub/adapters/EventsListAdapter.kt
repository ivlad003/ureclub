package com.ureclub.adapters
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ureclub.R
import com.ureclub.model.event.DataItemEvent
import com.ureclub.viewHolders.ViewHolderEventsListItem
import android.text.method.TextKeyListener.clear



class EventsListAdapter(internal var evetItems: List<DataItemEvent>, private val listener: (DataItemEvent) -> Unit):
        RecyclerView.Adapter<ViewHolderEventsListItem>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolderEventsListItem(
            LayoutInflater.from(parent.context).inflate(R.layout.item_event_list,
                    parent, false))

    override fun onBindViewHolder(holder: ViewHolderEventsListItem, position: Int) =
            holder.bind(evetItems[position],listener)

    override fun getItemCount() = evetItems.size
}