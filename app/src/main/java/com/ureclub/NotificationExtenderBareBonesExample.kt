package com.ureclub


import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationReceivedResult
import com.ureclub.activitys.Main


class NotificationExtenderBareBonesExample : NotificationExtenderService() {
    override fun onNotificationProcessing(notification: OSNotificationReceivedResult?): Boolean {
        val payload = notification?.payload
        val postIDs = payload?.additionalData?.getString("postIDs")
                ?.replace("[","")
                ?.replace("]","")?.split(",")

        val postType = payload?.additionalData?.getString("postType")

        Log.d("Notification","NotificationExtenderService\nNotification postID: ${postIDs} postType: ${postType}")

        val context = this.applicationContext
        var notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notifyIntent = Intent(this, Main::class.java)

        val title = "${postType}"
        notifyIntent.putExtra("title", title)
        notifyIntent.putExtra("notification", true)
        postIDs?.let {
            if (it.get(0).isNullOrEmpty()) {
                notifyIntent.putExtra("postID", it.get(0).toInt())
                notifyIntent.putExtra("postType", postType)
            }
        }
        notifyIntent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP

        val pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, 0)
        val res = this.resources

        var mNotification : Notification? = null

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            mNotification = Notification.Builder(this, "id")
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.ic_bim)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_app))
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .build()
        } else {

            mNotification = Notification.Builder(this)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.ic_bim)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.ic_app))
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle(title)
                    .build()

        }

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(11, mNotification)


        return false
    }
}