package com.ureclub.fragments.members

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.adapters.EventsListAdapter
import com.ureclub.adapters.PeapleAdapter
import com.ureclub.fragments.PeopleProfile
import com.ureclub.fragments.events.EventAbout
import com.ureclub.model.FilterEvent
import com.ureclub.model.FilterMembers
import com.ureclub.model.contacts.ContactsModel
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.model.event.DataItemEvent
import com.ureclub.model.filter.CategoryItem
import kotlinx.android.synthetic.main.members_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Peopels : Fragment() {
    var peopleList: List<PeopleItem> = ArrayList<PeopleItem>() as List<PeopleItem>
    var listsFilterEvent= FilterMembers(CategoryItem(), java.util.ArrayList())
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.members_list,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadData(listsFilterEvent)
        tryAgain.setOnClickListener {
            loadData(listsFilterEvent)
        }

        search.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filter(p0.toString())
            }
        })

        list.layoutManager = LinearLayoutManager(context)

    }

    private fun loadData(filterMembers: FilterMembers) {
        containerInfo.visibility = View.GONE
        progress.visibility = View.VISIBLE
        val token = "${App.profile.token}"
        App.Companion.api.allContacts(token)
                .enqueue(object : Callback<ContactsModel> {
                    override fun onFailure(call: Call<ContactsModel>?, t: Throwable?) {
                        containerInfo.visibility = View.VISIBLE
                        progress.visibility = View.GONE
                    }

                    override fun onResponse(call: Call<ContactsModel>?, response: Response<ContactsModel>?) {
                        containerInfo.visibility = View.GONE
                        progress.visibility = View.GONE
                        val model = response?.body()
                        model?.data?.let { data ->
                            data.people?.let { people ->
                                var list = people.filter {
                                    it.company
                                            ?.id != null
                                }

                                list = list.filter { it.type.equals("member") }

                                peopleList = list

                                peopleList = peopleList.filter {
                                    val listCategories: java.util.ArrayList<Integer> = java.util.ArrayList<Integer>()
                                    if (!filterMembers.categorysSelected.isEmpty()) {
                                        filterMembers.categorysSelected.forEach { itInt ->
                                            listCategories.add(itInt.id)
                                        }
                                        if(it.categories!=null){
                                            listCategories.containsAll(it.categories)
                                        }else{
                                            true
                                        }
                                    } else {
                                        true
                                    }
                                }

                                val id = filterMembers.sortBy.id
                                when (id) {
                                    Integer(1) -> {
                                        peopleList = peopleList
                                                .sortedWith(compareBy({ it.dateSince}))
                                    }
                                    Integer(2) -> {
                                        peopleList = peopleList
                                                .sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER
                                                        ,{it.firstName}))
                                    }
                                    Integer(3) -> {
                                        peopleList = peopleList
                                                .sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER
                                                        ,{it.firstName})).reversed()
                                    }
                                    else -> {
                                    }
                                }



                                setAdapter(peopleList)
                            }
                        }
                    }
                })
    }

    private fun setAdapter(peopleList: List<PeopleItem>) {
        val adapter = PeapleAdapter(peopleList) {
            val model = Gson().toJson(it)
            context?.let { it1 -> ActivityFragment.start(it1, PeopleProfile::class.java.name, model) }
        }
        list.adapter = adapter
    }

    private fun filter(text: String) {
        val filterdNames = ArrayList<PeopleItem>()
        for (p in peopleList) {
            val peopleStr ="${p.firstName}${p.lastName}${p.position}".toLowerCase()
            if(peopleStr.indexOf(text.toLowerCase())>-1){
                filterdNames.add(p)
            }
        }
        setAdapter(filterdNames)
    }

    private val mBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val filterEvent: FilterMembers = Gson()
                    .fromJson(intent.getStringExtra("model"), FilterMembers::class.java)
            listsFilterEvent = filterEvent
            loadData(listsFilterEvent)
        }
    }

    override fun onStart() {
        super.onStart()
        activity?.registerReceiver(mBroadCastReceiver, IntentFilter(
                "filterMembers"))
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(mBroadCastReceiver)
    }
}