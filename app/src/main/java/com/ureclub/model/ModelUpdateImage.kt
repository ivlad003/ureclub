package com.ureclub.model

import java.util.*

data class DataImage(val imageURL: String = "",
                val user: Int = 0)


data class ModelImageUpdate(val code: Object? = null,
                            val data: DataImage)


