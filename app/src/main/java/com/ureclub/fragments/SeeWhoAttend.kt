package com.ureclub.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.ureclub.App
import com.ureclub.R
import com.ureclub.activitys.ActivityFragment
import com.ureclub.adapters.PeapleAdapter
import com.ureclub.fragments.PeopleProfile
import com.ureclub.model.AnttendanceModel
import com.ureclub.model.contacts.ContactsModel
import com.ureclub.model.contacts.PeopleItem
import kotlinx.android.synthetic.main.see_who_attend_list.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SeeWhoAttend : Fragment() {
    var peopleList: List<PeopleItem> = ArrayList<PeopleItem>()
    var anttendanceModel: AnttendanceModel? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.see_who_attend_list,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        anttendanceModel = Gson().fromJson(arguments?.getString("model"), AnttendanceModel::class.java)

        loadData()
        tryAgain.setOnClickListener {
            loadData()
        }

        search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filter(p0.toString())
            }
        })

        list.layoutManager = LinearLayoutManager(context)

        leftButton.setOnClickListener {
            activity?.onBackPressed()
        }
        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)

        toolbar_title.text = ""
    }

    private fun loadData() {
        containerInfo.visibility = View.GONE
        progress.visibility = View.VISIBLE
        val token = "${App.profile.token}"
        App.Companion.api.allContacts(token)
                .enqueue(object : Callback<ContactsModel> {
                    override fun onFailure(call: Call<ContactsModel>?, t: Throwable?) {
                        containerInfo.visibility = View.VISIBLE
                        progress.visibility = View.GONE
                    }

                    override fun onResponse(call: Call<ContactsModel>?, response: Response<ContactsModel>?) {
                        containerInfo.visibility = View.GONE
                        progress.visibility = View.GONE
                        val model = response?.body()
                        model?.data?.let { data ->
                            data.people?.let { people ->
                                peopleList = people
                                val array = ArrayList<Int>()
                                anttendanceModel?.data?.forEach {
                                    array.add(it.person)
                                }
                                peopleList = peopleList.filter {
                                    array.indexOf(it.id) > -1
                                }
                                setAdapter(peopleList)
                            }
                        }
                    }
                })
    }

    private fun setAdapter(peopleList: List<PeopleItem>) {
        val adapter = PeapleAdapter(peopleList) {
            val model = Gson().toJson(it)
            context?.let { it1 -> ActivityFragment.start(it1, PeopleProfile::class.java.name, model) }
        }
        list.adapter = adapter
    }

    private fun filter(text: String) {
        val filterdNames = ArrayList<PeopleItem>()
        for (p in peopleList) {
            val peopleStr = "${p.firstName}${p.lastName}${p.position}".toLowerCase()
            if (peopleStr.indexOf(text.toLowerCase()) > -1) {
                filterdNames.add(p)
            }
        }
        setAdapter(filterdNames)
    }
}