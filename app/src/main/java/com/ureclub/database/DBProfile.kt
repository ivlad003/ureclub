package com.ureclub.database

import android.arch.persistence.room.*
import com.ureclub.model.Profile

@Dao
interface DBProfile {
    @Query("select * from profile limit 1")
    fun getProfile(): Profile

    @Insert
    fun insert(vararg users: Profile)

    @Delete
    fun delete(user: Profile)

    @Update
    fun update(user: Profile)
}