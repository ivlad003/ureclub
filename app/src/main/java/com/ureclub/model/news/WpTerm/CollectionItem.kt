package com.ureclub.model.news.WpTerm

import android.os.Parcel
import android.os.Parcelable

data class CollectionItem(val href: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(href)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CollectionItem> {
        override fun createFromParcel(parcel: Parcel): CollectionItem {
            return CollectionItem(parcel)
        }

        override fun newArray(size: Int): Array<CollectionItem?> {
            return arrayOfNulls(size)
        }
    }
}