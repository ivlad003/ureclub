package com.ureclub.model.event

import android.os.Parcel
import android.os.Parcelable

data class DataItemEvent(val date: Date?,
                         val image: String = "",
                         val ticket: Ticket?,
                         val ids: List<Integer>?,
                         val categories: List<Integer>?,
                         val location: Location?,
                         val id: Int = 0,
                         val post: Int = 0,
                         val title: String = "",
                         val content: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Date::class.java.classLoader),
            parcel.readString(),
            parcel.readParcelable(Ticket::class.java.classLoader),
            arrayListOf<Integer>().apply {
                parcel.readArrayList(Integer::class.java.classLoader)
            },
            arrayListOf<Integer>().apply {
                parcel.readArrayList(Integer::class.java.classLoader)
            },
            parcel.readParcelable(Location::class.java.classLoader),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(date, flags)
        parcel.writeString(image)
        parcel.writeList(ids)
        parcel.writeList(categories)
        parcel.writeParcelable(ticket, flags)
        parcel.writeParcelable(location, flags)
        parcel.writeInt(id)
        parcel.writeInt(post)
        parcel.writeString(title)
        parcel.writeString(content)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "DataItemEvent(date=$date, image='$image', ticket=$ticket, ids=$ids, categories=$categories, location=$location, id=$id, title='$title', content='$content')"
    }

    companion object CREATOR : Parcelable.Creator<DataItemEvent> {
        override fun createFromParcel(parcel: Parcel): DataItemEvent {
            return DataItemEvent(parcel)
        }

        override fun newArray(size: Int): Array<DataItemEvent?> {
            return arrayOfNulls(size)
        }
    }


}