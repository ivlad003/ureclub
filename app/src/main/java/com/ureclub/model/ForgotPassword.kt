package com.ureclub.model

data class ForgotPassword(val code: String = "",
                          val data: Data?)

data class Data(val user: User?)

data class User(val id: Int? = null,
                val email: String = "")