package com.ureclub.adapters

import android.support.v7.widget.RecyclerView
import android.view.View

import com.ureclub.R
import com.ureclub.model.event.DataItemEvent
import com.ureclub.model.filter.CategoryItem
import com.ureclub.viewHolders.*

import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

/**
 * Created by zvlad on 2/22/18.
 */

class FilterMembersAdapter(private val title: String, private val list: List<CategoryItem>, private  val listener: (CategoryItem) -> Unit) :
        StatelessSection(R.layout.item_header_filter, R.layout.item_filter) {
    override fun getContentItemsTotal(): Int {
        return list.size
    }

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        return ViewHolderMembersFilter(view)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val iHolder = holder as ViewHolderMembersFilter
        iHolder.bind(list[position],listener)
    }

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return ViewHolderHeaderFilter(view)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?) {
        val hHolder = holder as ViewHolderHeaderFilter?
        hHolder?.bind(title)
    }

    companion object {
        private val TAG = FilterMembersAdapter::class.java.simpleName
    }
}
