package com.ureclub.model.news

import android.os.Parcel
import android.os.Parcelable

data class Excerpt(val rendered: String = "",
                   val protected: Boolean = false): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readByte() != 0.toByte()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(rendered)
        parcel.writeByte(if (protected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Excerpt> {
        override fun createFromParcel(parcel: Parcel): Excerpt {
            return Excerpt(parcel)
        }

        override fun newArray(size: Int): Array<Excerpt?> {
            return arrayOfNulls(size)
        }
    }
}