package com.ureclub.model.contacts

import android.os.Parcel
import android.os.Parcelable

data class CompaniesItem(val phone: String = "",
                         val name: String = "",
                         val links: Links?,
                         val id: String = "",
                         val type: String = "",
                         val categories: ArrayList<Integer> = ArrayList(),
                         val email: String = ""):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            TODO("links"),
            parcel.readString(),
            parcel.readString(),
            parcel.readArrayList(Integer::class.java.classLoader) as ArrayList<Integer>,
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(phone)
        parcel.writeString(name)
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "CompaniesItem(phone='$phone', name='$name', links=$links, id='$id', type='$type', email='$email')"
    }

    companion object CREATOR : Parcelable.Creator<CompaniesItem> {
        override fun createFromParcel(parcel: Parcel): CompaniesItem {
            return CompaniesItem(parcel)
        }

        override fun newArray(size: Int): Array<CompaniesItem?> {
            return arrayOfNulls(size)
        }
    }


}