package com.ureclub.database

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import com.ureclub.model.Profile

@Database(entities = arrayOf(Profile::class), version = 2,exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun profileDao(): DBProfile
}