package com.ureclub.utils

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build

import java.util.Locale

/**
 * Created by zvlad on 2/24/18.
 */

object LocaleManager {
    public fun updateResources(context: Context, language: String): Context {
        var context = context
        val locale = Locale(language)
        Locale.setDefault(locale)

        val res = context.resources
        val config = Configuration(res.configuration)
        if (Build.VERSION.SDK_INT >= 17) {
            config.setLocale(locale)
            context = context.createConfigurationContext(config)
        } else {
            config.locale = locale
            res.updateConfiguration(config, res.displayMetrics)
        }
        return context
    }
}