package com.ureclub.activitys

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import com.ureclub.App
import com.ureclub.R
import com.ureclub.model.Profile
import com.ureclub.utils.AsteriskPasswordTransformationMethod
import com.ureclub.utils.MyBounceInterpolator
import kotlinx.android.synthetic.main.login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.os.AsyncTask
import android.view.KeyEvent
import android.widget.TextView
import android.widget.Toast
import com.ureclub.model.ProfileModel

class Login : BaseActivity() {
    fun login(view: View) {

        if (etPassword.text.toString().isEmpty() || etPassword.text.toString().length < 6) {
            etPassword.error = "Error, empty field or less than 6 characters."
        } else {
            etPassword.error = null
        }

        if (etEmail.text.toString().isEmpty()
                || !android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.text).matches()) {
            etEmail.error = "Error, empty field or wrong email."
        } else {
            etEmail.error = null
        }
        animationButton(view)
        sendLogin()
    }

    private fun animationButton(view: View) {
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        val interpolator = MyBounceInterpolator(0.1, 40.0)
        myAnim.interpolator = interpolator
        view.startAnimation(myAnim)
    }

    private fun sendLogin() {
        if (etEmail.error == null && etPassword.error == null) {
            progress.visibility = View.VISIBLE
            App.api.login(etEmail.text.toString(), etPassword.text.toString())
                    .enqueue(object : Callback<ProfileModel> {
                        override fun onFailure(call: Call<ProfileModel>?, t: Throwable?) {
                            Log.d("onFailure", "${call.toString()}")
                            Toast.makeText(this@Login,"${call.toString()}",Toast.LENGTH_SHORT).show()
                        }

                        override fun onResponse(call: Call<ProfileModel>?, response: Response<ProfileModel>?) {
                            progress.visibility = View.GONE

                            response?.body()?.data?.let {
                                AsyncTask.execute {
                                    it.token = "Bearer ${it.token}"
                                    Log.d("Login","token ${it.token}")
                                    App.db.profileDao().insert(it)
                                    runOnUiThread {
                                        finish()
                                        startActivity(Intent(App.context, Main::class.java))
                                    }
                                }
                            }


                            response?.errorBody()?.string()?.let{
                                if(it.indexOf("incorrect_password")>-1){
                                    Toast.makeText(this@Login,R.string.server_incorrect_password
                                            ,Toast.LENGTH_SHORT).show()
                                }else if(it.indexOf("invalid_email")>-1){
                                    Toast.makeText(this@Login,R.string.server_invalid_email
                                            ,Toast.LENGTH_SHORT).show()
                                }else{
                                    Toast.makeText(this@Login,R.string.server_error_undefined
                                            ,Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    })
        }
    }
//    {"code":"[jwt_auth] incorrect_password","message":"<strong>\u041e\u0428\u0418\u0411\u041a\u0410<\/strong>: \u0412\u0432\u0435\u0434\u0451\u043d\u043d\u044b\u0439 \u0432\u0430\u043c\u0438 \u043f\u0430\u0440\u043e\u043b\u044c \u0434\u043b\u044f \u0430\u0434\u0440\u0435\u0441\u0430 <strong>ivlad003@gmail.com<\/strong> \u043d\u0435\u0432\u0435\u0440\u0435\u043d. <a href=\"http:\/\/urec.1gb.ua\/wp-login.php?action=lostpassword\">\u0417\u0430\u0431\u044b\u043b\u0438 \u043f\u0430\u0440\u043e\u043b\u044c?<\/a>","data":{"status":403}}
    fun forgotPassword(view: View) {
        startActivity(Intent(App.Companion.context, ForgotPassword::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        etPassword.transformationMethod = AsteriskPasswordTransformationMethod()
        etPassword.setOnEditorActionListener(object: TextView.OnEditorActionListener {
            override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
                p0?.let { login(it) }
                return true
            }

        })
    }
}
