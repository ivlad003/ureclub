package com.ureclub.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ureclub.R
import com.ureclub.activitys.Main
import kotlinx.android.synthetic.main.code_of_ethics.*
import kotlinx.android.synthetic.main.main.*
import kotlinx.android.synthetic.main.toolbar_l_t_nan.*

class Notifications : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.code_of_ethics,
            container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        toolbar_title.text = getString(R.string.screen_notifications_title)
        leftButton.setOnClickListener {
            activity?.onBackPressed()
        }
        leftButton.setImageResource(R.drawable.ic_arrow_back_black_24dp)
    }
}