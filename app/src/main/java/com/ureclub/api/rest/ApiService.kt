package com.ureclub.api.rest


import com.ureclub.model.*
import com.ureclub.model.contacts.ContactsModel
import com.ureclub.model.event.EventModel
import com.ureclub.model.filter.Filter
import com.ureclub.model.newsNew.NewsModel
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @GET("wp-json/s4s_ureclub_rest/v1/news")
    fun listNews(@Header("Authorization") token: String,@Query("lang") lang:String): Call<NewsModel>

    @POST("wp-json/jwt-auth/v1/token")
    fun login(@Query("username") login: String, @Query("password") pass: String): Call<ProfileModel>

    @POST("s4s-reset-password.php")
    @FormUrlEncoded
    fun forgotPassword(@Field("email") login: String): Call<ForgotPassword>


    @GET("wp-json/s4s_ureclub_rest/v1/contacts")
    fun allContacts(@Header("Authorization") token: String): Call<ContactsModel>

    @GET("wp-json/s4s_ureclub_rest/v1/events")
    fun allEvents(@Header("Authorization") token: String,@Query("lang") lang:String): Call<EventModel>

    @GET("wp-json/s4s_ureclub_rest/v1/filter")
    fun allFilters(@Header("Authorization") token: String): Call<Filter>


    @POST("wp-json/s4s_ureclub_rest/v1/password-change")
    fun changePassword(@Header("Authorization") token: String, @Query("password") password: String): Call<Void>

    @Multipart
    @POST("wp-json/s4s_ureclub_rest/v1/photo-update")
    fun photoUpdate(@Header("Authorization") token: String,
                    @Part profileImage: MultipartBody.Part): Call<ModelImageUpdate>


    @POST("wp-json/s4s_ureclub_rest/v1/user-update")
    fun updateProfile(@Header("Authorization") token: String,
                      @Query("firstname") firstname: String,
                      @Query("lastname") lastname: String,
                      @Query("position") position: String,
                      @Query("email") email: String,
                      @Query("phone") phone: String,
                      @Query("hide_phone") hide_phone: Int,
                      @Query("facebook") facebook: String,
                      @Query("linkedin") linkedin: String,
                      @Query("description") description: String
    ): Call<UpdateProfile>


    @POST("wp-json/s4s_ureclub_rest/v1/attendance")
    fun attendance(@Header("Authorization") token: String,
                      @Query("event_id") eventId: Int
    ): Call<AnttendanceModel>


    @POST("wp-json/s4s_ureclub_rest/v1/book-event")
    fun book_event(@Header("Authorization") token: String,
                   @QueryMap options:Map<String, String>
    ): Call<ResponseBody>


    @POST("wp-json/s4s_ureclub_rest/v1/nonce")
    fun nonce(@Header("Authorization") token: String,
                      @Query("Authorization") Authorization: String,
                      @Query("nonce_action") nonce_action: String
    ): Call<ResponseBody>
}
