package com.ureclub.model

/**
 * Created by zvlad on 3/26/18.
 */
 enum class UpcomingPast {
    upcoming, past
}
