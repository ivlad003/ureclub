package com.ureclub.model

import java.util.*


data class UpdateProfile(val code: Object? = null,
                         val data: DataProfile)


data class DataProfile(val user: UserUp)


