package com.ureclub.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.ureclub.R
import com.ureclub.model.NewsModel
import com.ureclub.model.contacts.PeopleItem
import com.ureclub.model.news.ModelNews
import com.ureclub.viewHolders.*

class PeapleAdapter(private val peopleItems: List<PeopleItem>, private val listener: (PeopleItem) -> Unit):
        RecyclerView.Adapter<ViewHolderMember>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMember = ViewHolderMember(
            LayoutInflater.from(parent.context).inflate(R.layout.member_item,
                    parent, false))

    override fun onBindViewHolder(holder: ViewHolderMember, position: Int)=
            holder.bind(peopleItems[position],listener)

    override fun getItemCount() = peopleItems.size
}